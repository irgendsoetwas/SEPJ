package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.CollisionHandler;
import de.paluno.game.GameState;
import de.paluno.game.SEPGame;
import de.paluno.game.gameobjects.AirDrop;
import de.paluno.game.gameobjects.Boost;
import de.paluno.game.gameobjects.Luftschlag;
import de.paluno.game.gameobjects.Map;
import de.paluno.game.gameobjects.Mine;
import de.paluno.game.gameobjects.MovementBoost;
import de.paluno.game.gameobjects.PhysicsObject;
import de.paluno.game.gameobjects.Player;
import de.paluno.game.gameobjects.Projectile;
import de.paluno.game.gameobjects.ProjectileNeu;
import de.paluno.game.gameobjects.SecretWeapon;
import de.paluno.game.gameobjects.SentryGun;
import de.paluno.game.gameobjects.ShotDirectionIndicator;
import de.paluno.game.gameobjects.Sonderwaffe;
import de.paluno.game.gameobjects.Teleporter;
import de.paluno.game.gameobjects.Waffe;
import de.paluno.game.gameobjects.Waffe1;
import de.paluno.game.gameobjects.Waffe2;
import de.paluno.game.gameobjects.Water;
import de.paluno.game.gameobjects.WindIndicator;
import de.paluno.game.gameobjects.Worm;


public class PlayScreen extends com.badlogic.gdx.ScreenAdapter{
	public SEPGame game;
	public CollisionHandler collisionHandler;

	public SpriteBatch batch; 
	public static SpriteBatch hudBatch;
	public BitmapFont font, font2;
	private String text = "<Enter> for shoot mode", chooseWeapon = "press <G> to choose a weapon";
	public String windIndicator = "";
	private World world;
	public BitmapFont f=new BitmapFont();
	public String airdropText="";
	public GameState gamestate;
	public TextureRegion bgTexture;
	public Water water;

	private String player;
	private Color color = Color.RED;
	public Projectile p;
	private ShotDirectionIndicator sd;
	
	public SentryGun sentry;
	public ProjectileNeu missile;
	public boolean deleteBool = false;

	
	public boolean openPortal = false;
	public Teleporter teleporter;
	
	public Luftschlag luftschlag;
	public boolean startPlane = false;

	boolean mineDrop = false;
	public Mine mine;
	public Array<Mine> minen = new Array<Mine>();
	
	public boolean sentryHasShot;
	public boolean advancable;
	

	private boolean gamenotover=true;
	public boolean fired = false, needSd = true;
	public GameState lastState;
	public GameState lastlastState;

	public Box2DDebugRenderer debugRenderer;

	public Map map;
	private int mapType;
	private WindIndicator wind;

	private int stateCounter = 0;
	public int weapon;
	private int rcounter = 0;
	
	
	public Array<Player> players = new Array<Player>();
	public Array<Worm> wormsp1 = new Array<Worm>();
	public Array<Worm> wormsp2 = new Array<Worm>();
	public Array<Worm> wormsp3 = new Array<Worm>();
	public Array<Worm> wormsp4 = new Array<Worm>();
	public Array<Worm> wormsp5 = new Array<Worm>();
	
	private Array<Body> bodies = new Array<Body>();
	private Array<Body> bodies2 = new Array<Body>();
	public Array<Body> deleteMe = new Array<Body>();
	
	public Array<Texture> replayTextures = new Array<Texture>();
	private boolean showReplay = false;
	public SpriteBatch replayBatch = new SpriteBatch();
	private int replayCount = 148; 
	private boolean shouldCapture = true;
	
	private WeaponHUD weaponHUD;
	public boolean showWeapon = false;
	
	private Player lastplayer;
	private Player nowplayer;
	private int playercounter;
	private Worm wormc1;
	private Worm wormc2;
	private Worm wormc3;
	private Worm wormc4;
	private Worm wormc5;
	private int winnernum;

	private Array<Vector2> spawnPoints;
	private Array<Vector2>occupiedPoints=new Array <Vector2>();
	
	public Array <AirDrop> airdropProRunde = new Array <AirDrop>();//siqi
	public int gotWeapon = 0;
	
	public Array<Boost> boostArray = new Array<Boost>();
	public Array<MovementBoost> movementBoostArray = new Array<MovementBoost>();
	
	public PlayScreen (SEPGame game) {
		
		/* Initialisierung von Allgemeinem */
		this.game = game;
		world = new World(new Vector2(0, -98F ), true);
		
		batch = new SpriteBatch();
		hudBatch = new SpriteBatch();

		debugRenderer=new Box2DDebugRenderer();

		font = new BitmapFont();
		font2 = new BitmapFont();
		

		/* Initialisierung der Welt und Spiellogik */
		collisionHandler = new CollisionHandler(this);
		world.setContactListener(collisionHandler);
		gamestate = GameState.PLAYERTURN;
		water = new Water(this);

		/* Zuteilung der Map */
		mapType = game.settings.getMap();
		//mapType = MathUtils.random(3); // von 0 bis x
		map = new Map(this, mapType);
	
		/* Wuermer und Player erzeugen */
		int wormcounter = game.settings.getWormCounter();
		int playercounter = game.settings.getPlayerCounter();
		createPlayer(playercounter,wormcounter);
		
		nowplayer = players.random();
		player = nowplayer.playerName;

		weaponHUD = new WeaponHUD(this, players.get(0));
		spawnPoints = this.map.getSpawnPoints();
		spawnPoints.shuffle();
		
		
		/* Siyu Boost Erstellen */
		if(wormcounter >= 2) {
		    createBoost(wormcounter/2);
		    createMovementBoost(wormcounter/2);
		}
		if(wormcounter == 1) {
			createBoost(wormcounter);
			createMovementBoost(wormcounter);
		}
		removeOccupiedPoints();//remove spawn points occupied by boosts
		createAirdrop();//create airdrop siqi
		
		/* Background-Image */
		Texture tex = new Texture("bg.png");
		if (mapType == 2) tex = new Texture("maps/bgRobo.png");
		bgTexture = new TextureRegion(tex, 0, 0, Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		
	}

	// ----------------------------------------Getter & Setter ------------------------------------------------------------
	public World getWorld() {
		return world;
	}
	
	public void setShowReplay(boolean set) {
		this.showReplay = set;
	}
	
	// ----------------------------------------RENDER (GameLoop)------------------------------------------------------------
	public void render(float delta) {
		batch.setProjectionMatrix(game.camera.combined); //fuer kamera bewegen
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		/* Welt Physics werden angewandt */
		world.step(Gdx.graphics.getDeltaTime(), 6, 2); //velocityIterations and positionIterations

		/* Entfernen von Bodies nach dem world.step */
		entferneBloecke();
		entferneBloecke2();
		deleteBodies();
//		destroyBodies();
		entferneBloeckeDurchSentry();

		/* Background Image */
		batch.begin();
			batch.draw(bgTexture, game.camera.position.x - (bgTexture.getRegionWidth() / 2), game.camera.position.y - (bgTexture.getRegionHeight() / 2));
		batch.end();

		/* Rendering Tiles */
		batch.begin();
			map.getTmr().setView(game.camera);
			map.getTmr().render();
		batch.end();

		/* Rendering alles andere */
		batch.begin();
		
		/* GAME LOGIC HAPPENS HERE */
			checkWin();
			updatePhase(delta, gamestate);
			deleteBodies();
			renderPhase(delta);
			rcounter++;
			if (game.settings.getMitReplay()) {
				if (Gdx.input.isKeyJustPressed(Input.Keys.M)) showReplay = !showReplay;
				if (showReplay) startReplay(); 

				if (shouldCapture && rcounter % 2 == 0) captureScreen(); //Replay
			}
			
			if (Gdx.input.isKeyJustPressed(Input.Keys.G)) showWeapon = !showWeapon;
			if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) showWeapon = false;
			


		 //     debugRenderer.render(this.getWorld(), game.camera.combined); //kaka
			batch.end();

		/* Separater Batch fuer Textelemente die nicht von der Kamera beeinflusst werden*/
		hudBatch.begin();
			font.setColor(color);
			font.setColor(currentWorm().getColor());
			font.getData().setScale(1.5f);
//			font.draw(hudBatch, player + "'s turn", 800 - 120, 470);
			font.draw(hudBatch, player + "'s turn", Gdx.graphics.getWidth() - 250, Gdx.graphics.getHeight()-20);
			font.draw(hudBatch, text, 20, Gdx.graphics.getHeight()-20);
			f.setColor(Color.GREEN);
			f.getData().setScale(1.5f);
			font.getData().setScale(1.5f);
			f.draw(hudBatch,airdropText,Gdx.graphics.getWidth() / 2 - 80,Gdx.graphics.getHeight()-40);
			
			if(currentWorm().movePoint>100)
				   font2.setColor(Color.GREEN);
			else font2.setColor(Color.RED);
			font2.getData().setScale(1.5f);
			font2.draw(hudBatch, "Moves left:"+Integer.toString(currentWorm().movePoint), 10, Gdx.graphics.getHeight()-80);

			font.setColor(Color.GREEN);
			font.draw(hudBatch, chooseWeapon, 10, Gdx.graphics.getHeight()-50);
			
			/* WindIndicator */
			if (gamestate == GameState.SHOOTING) {
				airdropText="";
				wind.render(hudBatch, delta);
				wind.update(delta, gamestate);//dynamic wind indicator
			}
		hudBatch.end();
		
		if(showWeapon) {
			weaponHUD.render(hudBatch, delta);
		}
	}

	// ----------------------------------------RENDER (Images)--------------------------------------------------------------
	public void renderPhase(float delta) {
		water.render(batch, delta);
		
		map.render(batch, delta);
		
		if (sentry != null) {
			if (sentry.lifetime == 7) {
				sentry = null;
			}
		}
		
		if (sentry != null) {
			sentry.render(batch, delta);
			sentry.update(delta, gamestate);
			
			if (sentry.stateCounter % 2 == 0 && sentryHasShot == false) {
				Timer.schedule(new Task(){
					@Override
					public void run() {
						sentry.shoot();
					}
				}, 0.5f);
				
			sentryHasShot = true;
			}
		}
	

		if (Gdx.input.isKeyJustPressed(Input.Keys.O)) {
			game.setScreen(new MenuScreen(game));
		}

		// Invincible Boost rendern Siyu
		if (boostArray.size != 0) {
			for (int x = 0; x < boostArray.size; x++) {
				boostArray.get(x).render(batch, delta);
				boostArray.get(x).update(delta, gamestate);
			}
		}

		// Movement Boost rendern Siyu
		if (movementBoostArray.size != 0) {
			for (int x = 0; x < movementBoostArray.size; x++) {
				movementBoostArray.get(x).render(batch, delta);
				movementBoostArray.get(x).update(delta, gamestate);
			}
		}
		
		// Alle Wuermer rendern

		if (isEmpty(wormsp1)==false){
			for (int x = 0; x < wormsp1.size; x++) {
				wormsp1.get(x).render(batch, delta);
			}
		}
		if (isEmpty(wormsp2)==false) {
			for (int x = 0; x < wormsp2.size; x++) {
				wormsp2.get(x).render(batch, delta);
			}
		}
		if (isEmpty(wormsp3)==false) {
			for (int x = 0; x < wormsp3.size; x++) {
				wormsp3.get(x).render(batch, delta);
			}
		}
		if (isEmpty(wormsp4)==false) {
			for (int x = 0; x < wormsp4.size; x++) {
				wormsp4.get(x).render(batch, delta);
			}
		}
		if (isEmpty(wormsp5)==false) {
			for (int x = 0; x < wormsp5.size; x++) {
				wormsp5.get(x).render(batch, delta);
			}
		}
		
		//Airdrop render siqi
		if(gamestate==GameState.PLAYERTURN) {
			if(airdropProRunde.size!=0) {
				for(int a=0;a<airdropProRunde.size;a++) {
					airdropProRunde.get(a).render(batch, delta);
					airdropProRunde.get(a).update(delta,gamestate);
				}
			} 
		}

		if (gamestate == GameState.SHOOTING) {
			shootingPhase(delta);
		}

		//Projectile after shooting
		if (fired) {
			p.render(batch, delta); 
			p.update(delta, gamestate);
		}
		
		if(mineDrop) {
			for(Mine mine: minen) {
				mine.render(batch, delta);
				mine.update(delta, gamestate);
			}
		}
		
		infizieren();
		
		if (openPortal){
			teleporter.render(batch, delta);
		}
		if(startPlane){
			luftschlag.render(batch, delta);
		}
	}
	
	// ----------------------------------------SHOOTING + INFECT------------------------------------------------------------
	public void shootingPhase(float delta) {
		// SDI rendern
		if (needSd) sd.render(batch, delta);

		weapon = weaponHUD.getWeapon(); //ausgewaehlte Waffe

		//Siqi choose a weapon before shooting
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1) || weapon == 1) {
			chooseWeapon="weapon 1 chosen, <space> for shooting";
			weaponHUD.setWeapon(1);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2) || weapon == 2) {
			chooseWeapon="weapon 2 chosen, <space> for shooting";
			weaponHUD.setWeapon(2);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3) || weapon == 3) {
			chooseWeapon="weapon 3 chosen, <space> for shooting";
			weaponHUD.setWeapon(3);
		}
		//Judith
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4) || weapon == 4) {
			chooseWeapon="special weapon chosen, <space> for shooting";
			weaponHUD.setWeapon(4);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_5) || weapon == 5) {
			chooseWeapon="jetpack chosen, <space> for starting";
			weaponHUD.setWeapon(5);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_6) || weapon == 6) {
			chooseWeapon="mine chosen, <space> for planting";
			weaponHUD.setWeapon(6);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_7) || weapon == 7) {
			chooseWeapon="airstrike chosen, <space> for plane, then <enter> for shooting";
			weaponHUD.setWeapon(7);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_8) || weapon == 8) {
			chooseWeapon="teleport chosen, <space> to open portal, then <click> to teleport";
			weaponHUD.setWeapon(8);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_9) || weapon == 9) {
			chooseWeapon="sentry gun chosen, <space> for installation";
			weaponHUD.setWeapon(9);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_0) || weapon == 10) {
				if(gotWeapon!=0) {
				    chooseWeapon="secret weapon chosen, <space> for shooting";
				    weapon=10;
				    weaponHUD.setWeapon(10);
				}//secret weapon from airdrop siqi
				else
					chooseWeapon="press <G> to choose a weapon";
				weaponHUD.setWeapon(10);
		}

		if(Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
			
			
			if(weapon==1) {
				p = new Waffe(this, new Vector2(sd.getPosition()), sd.getAngle(),80,wind.speed,wind.getAngle());
				fired = true;
			}
			if(weapon==2) {
				p = new Waffe1(this, new Vector2(sd.getPosition()), sd.getAngle(),70,wind.speed,wind.getAngle());
				fired = true;
			}
			if(weapon==3) {
				p = new Waffe2(this, new Vector2(sd.getPosition()), sd.getAngle(),60,wind.speed,wind.getAngle());
				fired = true;
			}
			if(weapon==4 && currentPlayer().getWeaponCount(0) > 0) { //Sonderwaffe
				p = new Sonderwaffe(this, new Vector2(sd.getPosition()), sd.getAngle(),10,wind.speed,wind.getAngle());
				currentPlayer().useWeapon(0);
				fired = true;
			}
			if (weapon==5 && currentPlayer().getWeaponCount(1) > 0) { //Jetpack
				currentWorm().startJetpack();
				currentPlayer().useWeapon(1);
			}
			if (weapon==6 && currentPlayer().getWeaponCount(2) > 0) { //Mine
				mine = new Mine(this, getMinePosition());
				minen.add(mine);
				mine.droped = true;
				mineDrop = true;
				currentPlayer().useWeapon(2);
			}
			if (weapon==7 && currentPlayer().getWeaponCount(3) > 0) { //Airstrike
				luftschlag = new Luftschlag(this);
				startPlane = true;
				currentPlayer().useWeapon(3);
			}
			if (weapon==8 && currentPlayer().getWeaponCount(4) > 0) { //Teleport
				teleporter = new Teleporter(this, currentWorm());
				openPortal=true;
				currentPlayer().useWeapon(4);
			}
			if (weapon==9 && currentPlayer().getWeaponCount(5) > 0) { //Sentry gun
				//Standgesch�tz
				if(sentry == null) {
					if (currentWorm().standsOnGround == true &&
						currentWorm().sentryPlaced == false) {
						
						sentry = (new SentryGun(this, this.currentWorm()));
						currentPlayer().useWeapon(5);
						advanceGameState();
					}
				}
			}
			if (weapon==10 && gotWeapon != 0) {
				p = new SecretWeapon(this, sd.getPosition(), sd.getAngle(),100,wind.speed,wind.getAngle());
			    fired = true;
			}
			needSd = false;
			gotWeapon=0;
			createAirdrop();
		}
	}
	
	public void infizieren() {
		//Judith: Sonderwaffe - Radius fuer Infektion
		//Infizieren: gegnerischem Wurm
		for(int i = 0; i < wormsp1.size; i++) {
			if(wormsp1.get(i).getroffen) {
				for(int j=0; j<wormsp2.size; j++) {
					if(wormsp2.get(j).body.getPosition().x <= wormsp1.get(i).body.getPosition().x + 25 &&
							wormsp2.get(j).body.getPosition().x >= wormsp1.get(i).body.getPosition().x - 25 &&
							wormsp2.get(j).body.getPosition().y <= wormsp1.get(i).body.getPosition().y + 25 &&
							wormsp2.get(j).body.getPosition().y >= wormsp1.get(i).body.getPosition().y - 25) {
						wormsp2.get(j).getroffen = true;
					}
				}
				for(int k=0; k<wormsp3.size; k++) {
					if(wormsp3.get(k).body.getPosition().x <= wormsp1.get(i).body.getPosition().x + 25 &&
							wormsp3.get(k).body.getPosition().x >= wormsp1.get(i).body.getPosition().x - 25 &&
							wormsp3.get(k).body.getPosition().y <= wormsp1.get(i).body.getPosition().y + 25 &&
							wormsp3.get(k).body.getPosition().y >= wormsp1.get(i).body.getPosition().y - 25) {
						wormsp3.get(k).getroffen = true;
					}
				}
				for(int l=0; l<wormsp4.size;l++) {
					if(wormsp4.get(l).body.getPosition().x <= wormsp1.get(i).body.getPosition().x + 25 &&
							wormsp4.get(l).body.getPosition().x >= wormsp1.get(i).body.getPosition().x - 25 &&
							wormsp4.get(l).body.getPosition().y <= wormsp1.get(i).body.getPosition().y + 25 &&
							wormsp4.get(l).body.getPosition().y >= wormsp1.get(i).body.getPosition().y - 25) {
						wormsp4.get(l).getroffen = true;
					}
				}
				for(int m=0; m < wormsp5.size;m++) {
					if(wormsp5.get(m).body.getPosition().x <= wormsp1.get(i).body.getPosition().x + 25 &&
							wormsp5.get(m).body.getPosition().x >= wormsp1.get(i).body.getPosition().x - 25 &&
							wormsp5.get(m).body.getPosition().y <= wormsp1.get(i).body.getPosition().y + 25 &&
							wormsp5.get(m).body.getPosition().y >= wormsp1.get(i).body.getPosition().y - 25) {
						wormsp5.get(m).getroffen = true;
					}
				}
			}
		}
		for(int j=0; j<wormsp2.size; j++) {
			if(wormsp2.get(j).getroffen) {
				for(int i = 0; i < wormsp1.size; i++) {
					if(wormsp1.get(i).body.getPosition().x <= wormsp2.get(j).body.getPosition().x + 25 &&
							wormsp1.get(i).body.getPosition().x >= wormsp2.get(j).body.getPosition().x - 25 &&
							wormsp1.get(i).body.getPosition().y <= wormsp2.get(j).body.getPosition().y + 25 &&
							wormsp1.get(i).body.getPosition().y >= wormsp2.get(j).body.getPosition().y - 25) {
						wormsp1.get(i).getroffen = true;
					}
				}

				for(int k=0; k<wormsp3.size; k++) {
					if(wormsp3.get(k).body.getPosition().x <= wormsp2.get(j).body.getPosition().x + 25 &&
							wormsp3.get(k).body.getPosition().x >= wormsp2.get(j).body.getPosition().x - 25 &&
							wormsp3.get(k).body.getPosition().y <= wormsp2.get(j).body.getPosition().y + 25 &&
							wormsp3.get(k).body.getPosition().y >= wormsp2.get(j).body.getPosition().y - 25) {
						wormsp3.get(k).getroffen = true;
					}
				}

				for(int l=0; l<wormsp4.size;l++) {
					if(wormsp4.get(l).body.getPosition().x <= wormsp2.get(j).body.getPosition().x + 25 &&
							wormsp4.get(l).body.getPosition().x >= wormsp2.get(j).body.getPosition().x - 25 &&
							wormsp4.get(l).body.getPosition().y <= wormsp2.get(j).body.getPosition().y + 25 &&
							wormsp4.get(l).body.getPosition().y >= wormsp2.get(j).body.getPosition().y - 25) {
						wormsp4.get(l).getroffen = true;
					}
				}
				for(int m=0; m < wormsp5.size;m++) {
					if(wormsp5.get(m).body.getPosition().x <= wormsp2.get(j).body.getPosition().x + 25 &&
							wormsp5.get(m).body.getPosition().x >= wormsp2.get(j).body.getPosition().x - 25 &&
							wormsp5.get(m).body.getPosition().y <= wormsp2.get(j).body.getPosition().y + 25 &&
							wormsp5.get(m).body.getPosition().y >= wormsp2.get(j).body.getPosition().y - 25) {
						wormsp5.get(m).getroffen = true;
					}
				}
			}
		}
		for(int k=0; k<wormsp3.size; k++) {						
			if(wormsp3.get(k).getroffen) {
				for(int i = 0; i < wormsp1.size; i++) {
					if(wormsp1.get(i).body.getPosition().x <= wormsp3.get(k).body.getPosition().x + 25 &&
							wormsp1.get(i).body.getPosition().x >= wormsp3.get(k).body.getPosition().x - 25 &&
							wormsp1.get(i).body.getPosition().y <= wormsp3.get(k).body.getPosition().y + 25 &&
							wormsp1.get(i).body.getPosition().y >= wormsp3.get(k).body.getPosition().y - 25) {
						wormsp1.get(i).getroffen = true;
					}
				}
				for(int j = 0; j < wormsp2.size; j++) {
					if(wormsp2.get(j).body.getPosition().x <= wormsp3.get(k).body.getPosition().x + 25 &&
							wormsp2.get(j).body.getPosition().x >= wormsp3.get(k).body.getPosition().x - 25 &&
							wormsp2.get(j).body.getPosition().y <= wormsp3.get(k).body.getPosition().y + 25 &&
							wormsp2.get(j).body.getPosition().y >= wormsp3.get(k).body.getPosition().y - 25) {
						wormsp2.get(j).getroffen = true;
					}
				}
				for(int l = 0; l < wormsp4.size; l++) {
					if(wormsp4.get(l).body.getPosition().x <= wormsp3.get(k).body.getPosition().x + 25 &&
							wormsp4.get(l).body.getPosition().x >= wormsp3.get(k).body.getPosition().x - 25 &&
							wormsp4.get(l).body.getPosition().y <= wormsp3.get(k).body.getPosition().y + 25 &&
							wormsp4.get(l).body.getPosition().y >= wormsp3.get(k).body.getPosition().y - 25) {
						wormsp4.get(l).getroffen = true;
					}
				}
				for(int m = 0; m < wormsp5.size; m++) {
					if(wormsp5.get(m).body.getPosition().x <= wormsp3.get(k).body.getPosition().x + 25 &&
							wormsp5.get(m).body.getPosition().x >= wormsp3.get(k).body.getPosition().x - 25 &&
							wormsp5.get(m).body.getPosition().y <= wormsp3.get(k).body.getPosition().y + 25 &&
							wormsp5.get(m).body.getPosition().y >= wormsp3.get(k).body.getPosition().y - 25) {
						wormsp5.get(m).getroffen = true;
					}
				}
			}
		}
		for(int l = 0; l < wormsp4.size; l++) {
			if(wormsp4.get(l).getroffen) {
				for(int i = 0; i < wormsp1.size; i++) {
					if(wormsp1.get(i).body.getPosition().x <= wormsp4.get(l).body.getPosition().x + 25 &&
							wormsp1.get(i).body.getPosition().x >= wormsp4.get(l).body.getPosition().x - 25 &&
							wormsp1.get(i).body.getPosition().y <= wormsp4.get(l).body.getPosition().y + 25 &&
							wormsp1.get(i).body.getPosition().y >= wormsp4.get(l).body.getPosition().y - 25) {
						wormsp1.get(i).getroffen = true;
					}
				}
				for(int k=0; k<wormsp3.size; k++) {		
					if(wormsp3.get(k).body.getPosition().x <= wormsp4.get(l).body.getPosition().x + 25 &&
							wormsp3.get(k).body.getPosition().x >= wormsp4.get(l).body.getPosition().x - 25 &&
							wormsp3.get(k).body.getPosition().y <= wormsp4.get(l).body.getPosition().y + 25 &&
							wormsp3.get(k).body.getPosition().y >= wormsp4.get(l).body.getPosition().y - 25) {
						wormsp3.get(k).getroffen = true;
					}
				}
				for(int j = 0; j < wormsp2.size; j++) {
					if(wormsp2.get(j).body.getPosition().x <= wormsp4.get(l).body.getPosition().x + 25 &&
							wormsp2.get(j).body.getPosition().x >= wormsp4.get(l).body.getPosition().x - 25 &&
							wormsp2.get(j).body.getPosition().y <= wormsp4.get(l).body.getPosition().y + 25 &&
							wormsp2.get(j).body.getPosition().y >= wormsp4.get(l).body.getPosition().y - 25) {
						wormsp2.get(j).getroffen = true;
					}
				}
				for(int m = 0; m < wormsp5.size; m++) {
					if(wormsp5.get(m).body.getPosition().x <= wormsp4.get(l).body.getPosition().x + 25 &&
							wormsp5.get(m).body.getPosition().x >= wormsp4.get(l).body.getPosition().x - 25 &&
							wormsp5.get(m).body.getPosition().y <= wormsp4.get(l).body.getPosition().y + 25 &&
							wormsp5.get(m).body.getPosition().y >= wormsp4.get(l).body.getPosition().y - 25) {
						wormsp5.get(m).getroffen = true;
					}
				}
			}
		}
		for(int m = 0; m < wormsp5.size; m++) {
			if(wormsp5.get(m).getroffen) {
				for(int i = 0; i < wormsp1.size; i++) {
					if(wormsp1.get(i).body.getPosition().x <= wormsp5.get(m).body.getPosition().x + 25 &&
							wormsp1.get(i).body.getPosition().x >= wormsp5.get(m).body.getPosition().x - 25 &&
							wormsp1.get(i).body.getPosition().y <= wormsp5.get(m).body.getPosition().y + 25 &&
							wormsp1.get(i).body.getPosition().y >= wormsp5.get(m).body.getPosition().y - 25) {
						wormsp1.get(i).getroffen = true;
					}
				}
				for(int k=0; k<wormsp3.size; k++) {		
					if(wormsp3.get(k).body.getPosition().x <= wormsp5.get(m).body.getPosition().x + 25 &&
							wormsp3.get(k).body.getPosition().x >= wormsp5.get(m).body.getPosition().x - 25 &&
							wormsp3.get(k).body.getPosition().y <= wormsp5.get(m).body.getPosition().y + 25 &&
							wormsp3.get(k).body.getPosition().y >= wormsp5.get(m).body.getPosition().y - 25) {
						wormsp3.get(k).getroffen = true;
					}
				}
				for(int l = 0; l < wormsp4.size; l++) {
					if(wormsp4.get(l).body.getPosition().x <= wormsp5.get(m).body.getPosition().x + 25 &&
							wormsp4.get(l).body.getPosition().x >= wormsp5.get(m).body.getPosition().x - 25 &&
							wormsp4.get(l).body.getPosition().y <= wormsp5.get(m).body.getPosition().y + 25 &&
							wormsp4.get(l).body.getPosition().y >= wormsp5.get(m).body.getPosition().y - 25) {
						wormsp4.get(l).getroffen = true;
					}
				}
				for(int j = 0; j < wormsp2.size; j++) {
					if(wormsp2.get(j).body.getPosition().x <= wormsp5.get(m).body.getPosition().x + 25 &&
							wormsp2.get(j).body.getPosition().x >= wormsp5.get(m).body.getPosition().x - 25 &&
							wormsp2.get(j).body.getPosition().y <= wormsp5.get(m).body.getPosition().y + 25 &&
							wormsp2.get(j).body.getPosition().y >= wormsp5.get(m).body.getPosition().y - 25) {
						wormsp2.get(j).getroffen = true;
					}
				}

			}
		}


		//Infizieren: Wurm aus eigenem Team
		for(int i = 0; i < wormsp1.size; i++) {
			for(int j=0; j<wormsp1.size; j++) {
				if(wormsp1.get(i).getroffen && wormsp1.get(i) != wormsp1.get(j)) {
					if(wormsp1.get(j).body.getPosition().x <= wormsp1.get(i).body.getPosition().x + 25 &&
							wormsp1.get(j).body.getPosition().x >= wormsp1.get(i).body.getPosition().x - 25 &&
							wormsp1.get(j).body.getPosition().y <= wormsp1.get(i).body.getPosition().y + 25 &&
							wormsp1.get(j).body.getPosition().y >= wormsp1.get(i).body.getPosition().y - 25) {
						wormsp1.get(j).getroffen = true;
					}
				}
			}
		}
		for(int i = 0; i < wormsp2.size; i++) {
			for(int j=0; j<wormsp2.size; j++) {	
				if(wormsp2.get(i).getroffen && wormsp2.get(i) != wormsp2.get(j) ) {
					if(wormsp2.get(j).body.getPosition().x <= wormsp2.get(i).body.getPosition().x + 25 &&
							wormsp2.get(j).body.getPosition().x >= wormsp2.get(i).body.getPosition().x - 25 &&
							wormsp2.get(j).body.getPosition().y <= wormsp2.get(i).body.getPosition().y + 25 &&
							wormsp2.get(j).body.getPosition().y >= wormsp2.get(i).body.getPosition().y - 25) {
						wormsp2.get(j).getroffen = true;
					}
				}
			}
		}
		for(int i = 0; i < wormsp3.size; i++) {
			for(int j=0; j<wormsp3.size; j++) {	
				if(wormsp3.get(i).getroffen && wormsp3.get(i) != wormsp3.get(j) ) {
					if(wormsp3.get(j).body.getPosition().x <= wormsp3.get(i).body.getPosition().x + 25 &&
							wormsp3.get(j).body.getPosition().x >= wormsp3.get(i).body.getPosition().x - 25 &&
							wormsp3.get(j).body.getPosition().y <= wormsp3.get(i).body.getPosition().y + 25 &&
							wormsp3.get(j).body.getPosition().y >= wormsp3.get(i).body.getPosition().y - 25) {
						wormsp3.get(j).getroffen = true;
					}
				}
			}
		}
		for(int i = 0; i < wormsp4.size; i++) {
			for(int j=0; j<wormsp4.size; j++) {	
				if(wormsp4.get(i).getroffen && wormsp4.get(i) != wormsp4.get(j) ) {
					if(wormsp4.get(j).body.getPosition().x <= wormsp4.get(i).body.getPosition().x + 25 &&
							wormsp4.get(j).body.getPosition().x >= wormsp4.get(i).body.getPosition().x - 25 &&
							wormsp4.get(j).body.getPosition().y <= wormsp4.get(i).body.getPosition().y + 25 &&
							wormsp4.get(j).body.getPosition().y >= wormsp4.get(i).body.getPosition().y - 25) {
						wormsp4.get(j).getroffen = true;
					}
				}
			}
		}
		for(int i = 0; i < wormsp5.size; i++) {
			for(int j=0; j<wormsp5.size; j++) {	
				if(wormsp5.get(i).getroffen && wormsp5.get(i) != wormsp5.get(j) ) {
					if(wormsp5.get(j).body.getPosition().x <= wormsp5.get(i).body.getPosition().x + 25 &&
							wormsp5.get(j).body.getPosition().x >= wormsp5.get(i).body.getPosition().x - 25 &&
							wormsp5.get(j).body.getPosition().y <= wormsp5.get(i).body.getPosition().y + 25 &&
							wormsp5.get(j).body.getPosition().y >= wormsp5.get(i).body.getPosition().y - 25) {
						wormsp5.get(j).getroffen = true;
					}
				}
			}
		}
	}
	
	public void mineExplodiere(final Mine mine){ //Judith
		for(int i = 0; i < wormsp1.size; i++) {
			if(wormsp1.get(i).body.getPosition().x <= mine.position.x + 40 &&
					wormsp1.get(i).body.getPosition().x >= mine.position.x - 40 &&
					wormsp1.get(i).body.getPosition().y <= mine.position.y + 40 &&
					wormsp1.get(i).body.getPosition().y >= mine.position.y - 40) {
				System.out.println("worm " + wormsp1.get(i).body.getPosition().x + "mine: " + mine.position.x);
				System.out.println("worm " + wormsp1.get(i).body.getPosition().y + "mine: " + mine.position.y);
				wormsp1.get(i).setHealth(wormsp1.get(i).getHealth()-50);
				mine.explodiert = true;
				if(wormsp1.get(i).getHealth()<=0) {
					wormsp1.get(i).die();
				}
				
			}
		}
		for(int i = 0; i < wormsp2.size; i++) {
			if(wormsp2.get(i).body.getPosition().x <= mine.position.x + 40 &&
					wormsp2.get(i).body.getPosition().x >= mine.position.x - 40 &&
					wormsp2.get(i).body.getPosition().y <= mine.position.y + 40 &&
					wormsp2.get(i).body.getPosition().y >= mine.position.y - 40) {
				wormsp2.get(i).setHealth(wormsp2.get(i).getHealth()-50);
				mine.explodiert = true;
				if(wormsp2.get(i).getHealth()<=0) {
					wormsp2.get(i).die();
				}
				
			}
		}
		for(int i = 0; i < wormsp3.size; i++) {
			if(wormsp3.get(i).body.getPosition().x <= mine.position.x + 40 &&
					wormsp3.get(i).body.getPosition().x >= mine.position.x - 40 &&
					wormsp3.get(i).body.getPosition().y <= mine.position.y + 40 &&
					wormsp3.get(i).body.getPosition().y >= mine.position.y - 40) {
				wormsp3.get(i).setHealth(wormsp3.get(i).getHealth()-50);
				mine.explodiert = true;
				if(wormsp3.get(i).getHealth()<=0) {
					wormsp3.get(i).die();
				}
				
			}
		}
		for(int i = 0; i < wormsp4.size; i++) {
			if(wormsp4.get(i).body.getPosition().x <= mine.position.x + 40 &&
					wormsp4.get(i).body.getPosition().x >= mine.position.x - 40 &&
					wormsp4.get(i).body.getPosition().y <= mine.position.y + 40 &&
					wormsp4.get(i).body.getPosition().y >= mine.position.y - 40) {
				wormsp4.get(i).setHealth(wormsp4.get(i).getHealth()-50);
				mine.explodiert = true;
				if(wormsp4.get(i).getHealth()<=0) {
					wormsp4.get(i).die();
				}
			
			}
		}
		for(int i = 0; i < wormsp5.size; i++) {
			if(wormsp5.get(i).body.getPosition().x <= mine.position.x + 40 &&
					wormsp5.get(i).body.getPosition().x >= mine.position.x - 40 &&
					wormsp5.get(i).body.getPosition().y <= mine.position.y + 40 &&
					wormsp5.get(i).body.getPosition().y >= mine.position.y - 40) {
				wormsp5.get(i).setHealth(wormsp5.get(i).getHealth()-50);
				mine.explodiert = true;
				if(wormsp5.get(i).getHealth()<=0) {
					wormsp5.get(i).die();
				}
				
			}
		}

		if(mine.explodiert) {
			mine.explode();
			Timer.schedule(new Task(){
				@Override
				public void run() {
					deleteMe.add(mine.body);
					minen.removeValue(mine, true); //true: == & false: equals() comparison will be used
				}
			}, 1); 
		}
	}
	
	//Judith
	public Vector2 getMinePosition(){
		Vector2 pos;
		if(sd.getPosition().x < currentWorm().getBody().getPosition().x){

			pos = sd.getPosition().sub(15, 15);

		}
		else {
			pos = sd.getPosition().add(15, 15);
		}
		return pos;

	}

	
	// ----------------------------------------UPDATE (Movement, Camera)----------------------------------------------------
	public void updatePhase(float delta,GameState gamestate) {
		movingCamera();

		/* Updating + Camera focus + Player name + color */
		if (gamestate == GameState.PLAYERTURN) {
			currentWorm().update(delta, gamestate);
			setCameraFocus(currentWorm());
		}
		
		player = currentWorm().getPlayername();
		
		checkWin();
		
		/* Advancing to Shooting mode and creating SDI */
		if (!showWeapon) {
			if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && gamestate != GameState.SHOOTING) {
				needSd = true;
				sd = new ShotDirectionIndicator(currentWorm()); 
				wind = new WindIndicator(this);
				
				//siyu reset movepoint
				if(currentWorm().isSpeedUp == true) {
					currentWorm().movePoint = 1000;
				}
				else {
					currentWorm().movePoint = 500;
				}
				
				for(int i = 0; i < boostArray.size; i++) {
					if(boostArray.get(i).hp <= 0) {
						deleteBoost(boostArray.get(i));
					}
				}
				
				for(int i = 0; i < movementBoostArray.size; i++) {
					if(movementBoostArray.get(i).hp <= 0) {
						deleteMovementBoost(movementBoostArray.get(i));
					}
				}
				
				for(int i=0;i<airdropProRunde.size;i++) {
					this.deleteMe.add(airdropProRunde.get(i).body);
				}
				airdropProRunde.clear();//remove all airdrops siqi
				currentWorm().gotAirdrop=false;
				

				advanceGameState();
			}
		}
	}
	
	// ----------------------------------------Gamestate Handler------------------------------------------------------------
	public int checkLeereWorms(Player p) {
		int leere =0;
		for (int i=p.playerNumber-1; i < players.size; i++) {
			if (players.get(i).getWorm().size==0) {
				leere++;;
			}else break;
		}
		return leere;
	}

	public void advanceWorm() {

		switch (nowplayer.getPlayerNumber()) {
		case 1:
			if (wormc1.getNumberOfWorm()+2 > wormsp1.size) {
				wormc1 = wormsp1.first();
			}
			else wormc1 = wormsp1.get(wormc1.getNumberOfWorm()+1);
			break;
		case 2:
			if (wormc2.getNumberOfWorm()+2 > wormsp2.size) {
				wormc2 = wormsp2.first();
			}
			else wormc2 = wormsp2.get(wormc2.getNumberOfWorm()+1);
			break;
		case 3:
			if (wormc3.getNumberOfWorm()+2 > wormsp3.size) {
				wormc3 = wormsp3.first();
			}
			else wormc3 = wormsp3.get(wormc3.getNumberOfWorm()+1);
			break;
		case 4:
			if (wormc4.getNumberOfWorm()+2 > wormsp4.size) {
				wormc4 = wormsp4.first();
			}
			else wormc4 = wormsp4.get(wormc4.getNumberOfWorm()+1);
			break;
		case 5:
			if (wormc5.getNumberOfWorm()+2 > wormsp5.size) {
				wormc5 = wormsp5.first();
			}
			else wormc5 = wormsp5.get(wormc5.getNumberOfWorm()+1);
			break;
		}
	}
	
	public void advancePlayer() {
		lastplayer = nowplayer;

		if (lastplayer.getPlayerNumber()+1 > players.size) {
			nowplayer = players.first();
		}
		else if (players.get(lastplayer.getPlayerNumber()).getWorm().size==0){
			nowplayer = players.get(lastplayer.getPlayerNumber()+checkLeereWorms(lastplayer));
		}
		else nowplayer = players.get(lastplayer.getPlayerNumber());
		
		player = currentWorm().getPlayername();
	}

	public void advanceGameState() { //This method is the update step of the play screens game loop.
		sentryHasShot = false;
		sentry.stateCounter++;
		
		if(sentry != null) {
		sentry.lifetime++;
		}
		
		GameState[] states = {GameState.PLAYERTURN, GameState.SHOOTING};
		
//		System.out.println("---------------");
//		System.out.println(stateCounter);
		
		if (gamenotover) {
			if (stateCounter==0) stateCounter=1;
			else if (stateCounter==1) { 
				stateCounter=0;
				advancePlayer();
				advanceWorm();
			}
		}

		lastState = gamestate;
		gamestate = states[stateCounter];
		
//		System.out.println(stateCounter);
//		System.out.println("----------------");
		
		if (gamestate == GameState.PLAYERTURN) { 
			text = "<Enter> for shoot mode"; 
			water.wasserHoch();
			weaponHUD = new WeaponHUD(this, currentPlayer());
			
		    if (currentWorm().invincibleRound > 0) {
			    currentWorm().invincible();
			    currentWorm().invincibleRound--;
		    }//siyu invincible round
		    if (currentWorm().invincibleRound <= 0) {
			    currentWorm().removeInvincible();
		    }
			
			
			 for(int a=0;a<wormsp1.size;a++) {//hp+20 band-aid siqi
	    	       if(wormsp1.get(a).round>0)
	    		      wormsp1.get(a).useBandAid();}
	         for(int b=0;b<wormsp2.size;b++) {
	  	           if(wormsp2.get(b).round>0)
	  		          wormsp2.get(b).useBandAid(); }
	         for(int b=0;b<wormsp3.size;b++) {
	  	           if(wormsp3.get(b).round>0)
	  		          wormsp3.get(b).useBandAid(); }
	         for(int b=0;b<wormsp4.size;b++) {
	  	           if(wormsp4.get(b).round>0)
	  		          wormsp4.get(b).useBandAid(); }
	         for(int b=0;b<wormsp5.size;b++) {
	  	           if(wormsp5.get(b).round>0)
	  		          wormsp5.get(b).useBandAid(); }
	         
			
			//Judith Health-10 pro Runde
			for(int i = 0; i < wormsp1.size; i++) {
				if(wormsp1.get(i).getroffen) {
					wormsp1.get(i).setHealth(wormsp1.get(i).getHealth()-10);
					wormsp1.get(i).runden();
					if(wormsp1.get(i).getHealth()<=0) {
						wormsp1.get(i).die();
					}
				}
			}
			for(int j=0; j<wormsp2.size; j++) {
				if(wormsp2.get(j).getroffen) {
					wormsp2.get(j).setHealth(wormsp2.get(j).getHealth()-10);
					wormsp2.get(j).runden();
					if(wormsp2.get(j).getHealth()<=0) {
						wormsp2.get(j).die();
					}
				}
			}
			for(int j=0; j<wormsp3.size; j++) {
				if(wormsp3.get(j).getroffen) {
					wormsp3.get(j).setHealth(wormsp3.get(j).getHealth()-10);
					wormsp3.get(j).runden();
					if(wormsp3.get(j).getHealth()<=0) {
						wormsp3.get(j).die();
					}
				}
			}
			for(int j=0; j<wormsp4.size; j++) {
				if(wormsp4.get(j).getroffen) {
					wormsp4.get(j).setHealth(wormsp4.get(j).getHealth()-10);
					wormsp4.get(j).runden();
					if(wormsp4.get(j).getHealth()<=0) {
						wormsp4.get(j).die();
					}
				}
			}
			for(int j=0; j<wormsp5.size; j++) {
				if(wormsp5.get(j).getroffen) {
					wormsp5.get(j).setHealth(wormsp5.get(j).getHealth()-10);
					wormsp5.get(j).runden();
					if(wormsp5.get(j).getHealth()<=0) {
						wormsp5.get(j).die();
					}
				}
			}
		}
		else { 
			chooseWeapon = "press <G> to choose a weapon";
		}
	}
		
	public void setGameState(GameState gameState) {
		this.gamestate = gameState;
	}

	public boolean eineRundeVorbei() {
		if(lastState == GameState.SHOOTING && gamestate == GameState.PLAYERTURN) {
			return true;
		}
		return false;
	}

	// ----------------------------------------CAMERA-----------------------------------------------------------------------
	public void movingCamera() {
		// Movement of camera not working when locked to an object
		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			game.camera.translate(-3, 0, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.D)) {
			game.camera.translate(3, 0, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.S)) {
			game.camera.translate(0, -3, 0);
		}
		if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			game.camera.translate(0, 3, 0);
		}
		
		/* ZOOMING */
		if (Gdx.input.isKeyPressed(Input.Keys.Q)&& game.camera.zoom < 1.2f) {
			game.camera.zoom += 0.02;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.E)&& game.camera.zoom > 0.36) {
			game.camera.zoom -= 0.02;
		}
		game.camera.update();
	}
	
	public void setCameraFocus(PhysicsObject gameObject) {
		game.camera.position.x = gameObject.getBody().getPosition().x;
		game.camera.position.y = gameObject.getBody().getPosition().y;
		game.camera.zoom = 0.5f;
		game.camera.update();
	}
	
	private void captureScreen() {
		Texture tex = getScreenshot();
		
		replayTextures.insert(0, tex);
		
		replayCount = replayTextures.size-1;
		
		if (replayTextures.size == 150) replayTextures.removeIndex(149);
		
		replayCount = replayTextures.size-1;
	}
	
	public Texture getScreenshot(){
	    Texture texture = new Texture(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), Pixmap.Format.RGB888);

	    Gdx.gl.glEnable(GL20.GL_TEXTURE_2D);
	    Gdx.gl.glActiveTexture(GL20.GL_TEXTURE0);
	    texture.bind();
	    Gdx.gl.glCopyTexImage2D(GL20.GL_TEXTURE_2D, 0, GL20.GL_RGB, 0, 0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0);
	    Gdx.gl.glDisable(GL20.GL_TEXTURE_2D);

	    return texture;
	}
	
	public void startReplay() {
		shouldCapture = false;
		
		replayBatch.begin();
		
		//replayBatch.draw(replayTextures.get(replayCount), 0, 0);
		Sprite sprite = new Sprite(replayTextures.get(replayCount));
	    sprite.flip(false, true);
	    sprite.draw(replayBatch);
		replayCount--;
		
		if (replayCount == 0) {
			showReplay = false;
			replayCount = 148;
			replayTextures.clear();
			shouldCapture = true;
		}
		
		replayBatch.end();

	}
	
	//----------------------------------------Airdrop (Create,Delete) Siqi-----------------------------------------------
	public void createAirdrop() {
//		deleteBodies();
		world.step(Gdx.graphics.getDeltaTime(), 6, 2);
		spawnPoints.shuffle();
		for(int i=1;i<4;i++)
		airdropProRunde.add(new AirDrop(this,i,this.spawnPoints.get(spawnPoints.size-i)));
	}
	
	public void deleteAirdrop(AirDrop airdrop) {
			if(airdropProRunde.size==1&&airdropProRunde.get(0).equals(airdrop))
				airdropProRunde.clear();
			else {
				for (int i = 0; i<airdropProRunde.size; i++) {
				  if (airdropProRunde.get(i).equals(airdrop)) {
					airdropProRunde.removeIndex(i);}
			    }
			}
			deleteMe.add(airdrop.getBody());
			airdrop.setBodyToNullReference();
		}
	
	public void removeOccupiedPoints() {//remove spawnpoints occupied by boots
		for(int i=0;i<boostArray.size;i++) {
			occupiedPoints.add(boostArray.get(i).spawn);
		}
		for(int j=0;j<movementBoostArray.size;j++) {
			occupiedPoints.add(movementBoostArray.get(j).spawn);
		}
		for(int a=0;a<spawnPoints.size;a++) {
			for(int b=0;b<occupiedPoints.size;b++) {
				if(spawnPoints.get(a).equals(occupiedPoints.get(b))) {
					spawnPoints.removeIndex(a);
					a--;
				}
			}
		}
	}

	// ----------------------------------------INVINCIBLE BOOST (Create,Delete) Siyu-----------------------------------------------
	public void createBoost(int boostAnzahl) { 
		int v =0;
		while (v < boostAnzahl) {
			boostArray.add(new Boost(this,this.spawnPoints.get((spawnPoints.size-v-1))));

			v++;
		}	
	}

	public void deleteBoost(Boost boosti) {
		if (boostArray.size==1 && boostArray.get(0).equals(boosti)) {
			boostArray.clear();
		}
		else {
			for (int i = 0; i<boostArray.size; i++) {
				if (boostArray.get(i).equals(boosti)) {
					boostArray.removeIndex(i);
				}
				System.out.println("boostArraySize: " + boostArray.size);
			}
		}
		deleteMe.add(boosti.getBody());
		//boosti.setBodyToNullReference();
	}

	// ----------------------------------------MOVEMENT BOOST (Create,Delete) Siyu-----------------------------------------------
	public void createMovementBoost(int movementBoostAnzahl) { 
		int v =0;
		while (v < movementBoostAnzahl) {
			movementBoostArray.add(new MovementBoost(this,this.spawnPoints.get(spawnPoints.size-3-v)));
			v++;
		}	
	}

	public void deleteMovementBoost(MovementBoost movementBoosti) {
		if (movementBoostArray.size==1 && movementBoostArray.get(0).equals(movementBoosti)) {
			movementBoostArray.clear();
		}
		else {
			for (int i = 0; i<movementBoostArray.size; i++) {
				if (movementBoostArray.get(i).equals(movementBoosti)) {
					movementBoostArray.removeIndex(i);
				}
				System.out.println("movementBoostArrayArraySize: " + movementBoostArray.size);
			}
		}
		deleteMe.add(movementBoosti.getBody());	
		//movementBoosti.setBodyToNullReference();
	}

	// ----------------------------------------WORM (Create, Current, Delete) -----------------------------------------------
	public void createPlayer(int pc,int wc) { //Anzahlderwormer ubergeben und jetzt sollen wormer erstellt und in array ubergeben werden
		int v=0;

		switch (pc) {
		case 2: 
			v =0;
			while (v<wc) {
				wormsp1.add(new Worm(1,v,this));
				wormsp2.add(new Worm(2,v,this));
				v++;
			}	
			players.add(new Player(1,wormsp1));
			players.add(new Player(2,wormsp2));
			
			wormc1 = players.get(0).getWorm().get(0);
			wormc2 = players.get(1).getWorm().get(0);
			break;
		case 3:
			v =0;
			while (v<wc) {
				wormsp1.add(new Worm(1,v,this));
				wormsp2.add(new Worm(2,v,this));
				wormsp3.add(new Worm(3,v,this));
				v++;
			}	
			players.add(new Player(1,wormsp1));
			players.add(new Player(2,wormsp2));
			players.add(new Player(3,wormsp3));
			
			wormc1 = players.get(0).getWorm().get(0);
			wormc2 = players.get(1).getWorm().get(0);
			wormc3 = players.get(2).getWorm().get(0);
			break;
		case 4:
			v =0;
			while (v<wc) {
				wormsp1.add(new Worm(1,v,this));
				wormsp2.add(new Worm(2,v,this));
				wormsp3.add(new Worm(3,v,this));
				wormsp4.add(new Worm(4,v,this));
				v++;
			}	
			players.add(new Player(1,wormsp1));
			players.add(new Player(2,wormsp2));
			players.add(new Player(3,wormsp3));
			players.add(new Player(4,wormsp4));
			
			wormc1 = players.get(0).getWorm().get(0);
			wormc2 = players.get(1).getWorm().get(0);
			wormc3 = players.get(2).getWorm().get(0);
			wormc4 = players.get(3).getWorm().get(0);
			break;
		case 5:
			v =0;
			while (v<wc) {
				wormsp1.add(new Worm(1,v,this));
				wormsp2.add(new Worm(2,v,this));
				wormsp3.add(new Worm(3,v,this));
				wormsp4.add(new Worm(4,v,this));
				wormsp5.add(new Worm(5,v,this));
				v++;
			}	
			players.add(new Player(1,wormsp1));
			players.add(new Player(2,wormsp2));
			players.add(new Player(3,wormsp3));
			players.add(new Player(4,wormsp4));
			players.add(new Player(5,wormsp5));
			
			wormc1 = players.get(0).getWorm().first();
			wormc2 = players.get(1).getWorm().get(0);
			wormc3 = players.get(2).getWorm().get(0);
			wormc4 = players.get(3).getWorm().get(0);
			wormc5 = players.get(4).getWorm().get(0);
			break;

		}
}
	
	public Worm currentWorm() { // Gibt den Wurm des Spielers der am Zug ist wieder

		checkWin();
		switch (nowplayer.getPlayerNumber()) {
		case 1:
			if (isEmpty(wormsp1)==false) {
				for (int i=0; i< wormsp1.size; i++) {
					if (wormsp1.get(i).equals(wormc1))
						return wormsp1.get(i);
				}
				if (wormc1.getNumberOfWorm()+2 > wormsp1.size) {
					return wormsp1.first();
				}
				else return wormsp1.get(wormc1.getNumberOfWorm()+1);
			}

		case 2:
			if (isEmpty(wormsp2)==false) {
				for (int i=0; i< wormsp2.size; i++) {
					if (wormsp2.get(i).equals(wormc2))
						return wormsp2.get(i);
				}
				if (wormc2.getNumberOfWorm()+2 > wormsp2.size) {
					return wormsp2.first();
				}
				else return wormsp2.get(wormc2.getNumberOfWorm()+1);
			}
		case 3:
			if (isEmpty(wormsp3)==false) {
				for (int i=0; i< wormsp3.size; i++) {
					if (wormsp3.get(i).equals(wormc3))
						return wormsp3.get(i);
				}
				if (wormc3.getNumberOfWorm()+2 > wormsp3.size) {
					return wormsp3.first();
				}
				else return wormsp3.get(wormc3.getNumberOfWorm()+1);
			}
		case 4:
			if (isEmpty(wormsp4)==false) {
				for (int i=0; i< wormsp4.size; i++) {
					if (wormsp4.get(i).equals(wormc4))
						return wormsp4.get(i);
				}
				if (wormc4.getNumberOfWorm()+2 > wormsp4.size) {
					return wormsp4.first();
				}
				else return wormsp4.get(wormc4.getNumberOfWorm()+1);
			}
		case 5:
			if (isEmpty(wormsp5)==false) {
				for (int i=0; i< wormsp5.size; i++) {
					if (wormsp5.get(i).equals(wormc5))
						return wormsp5.get(i);
				}
				if (wormc5.getNumberOfWorm()+2 > wormsp5.size) {
					return wormsp5.first();
				}
				else return wormsp5.get(wormc5.getNumberOfWorm()+1);
			}
		}
		System.out.println("muss bleiben");
		if (isEmpty(wormsp1)==false) {
			return wormsp1.first();
		}else if (isEmpty(wormsp2)==false) {
			return wormsp2.first();
		}else if (isEmpty(wormsp3)==false) {
			return wormsp3.first();
		}else if (isEmpty(wormsp4)==false) {
			return wormsp4.first();
		}else if (isEmpty(wormsp5)==false) {
			return wormsp5.first();
		}
		System.out.println("HI Fehler");
		return null;
	}

	private Player currentPlayer() {
		return nowplayer;
	}

	/* For deleting worms */
	public void deleteWorm(Worm wurmi) {
		if (wormsp1.size==1 && wormsp1.get(0).equals(wurmi)) {
			wormsp1.clear();
		}
		else {
			for (int i = 0; i<wormsp1.size; i++) {
				if (wormsp1.get(i).equals(wurmi)) {
					wormsp1.removeIndex(i);
				}
				System.out.println("Wormsp1Size: " + wormsp1.size);
			}
		}

		if (wormsp2.size==1 && wormsp2.get(0).equals(wurmi)) {
			wormsp2.clear();
		}
		else {
			for (int i = 0; i<wormsp2.size; i++) {
				if (wormsp2.get(i).equals(wurmi)) {
					wormsp2.removeIndex(i);
				}
				System.out.println("Wormsp2Size: "+wormsp2.size);
			}
		}
		if (wormsp3.size==1 && wormsp3.get(0).equals(wurmi)) {
			wormsp3.clear();
		}
		else {
			for (int i = 0; i<wormsp3.size; i++) {
				if (wormsp3.get(i).equals(wurmi)) {
					wormsp3.removeIndex(i);
				}
				System.out.println("Wormsp3Size: "+wormsp3.size);
			}
		}
		if (wormsp4.size==1 && wormsp4.get(0).equals(wurmi)) {
			wormsp4.clear();
		}
		else {
			for (int i = 0; i<wormsp4.size; i++) {
				if (wormsp4.get(i).equals(wurmi)) {
					wormsp4.removeIndex(i);
				}
				System.out.println("Wormsp4Size: "+wormsp4.size);
			}
		}
		if (wormsp5.size==1 && wormsp5.get(0).equals(wurmi)) {
			wormsp5.clear();
		}
		else {
			for (int i = 0; i<wormsp5.size; i++) {
				if (wormsp5.get(i).equals(wurmi)) {
					wormsp5.removeIndex(i);
				}
				System.out.println("Wormsp5Size: "+wormsp5.size);
			}
		}
		deleteMe.add(wurmi.getBody());
		checkWin();
	}

	private boolean isEmpty(Array<Worm> j) { //returns true empty wenn array ist leer
		if (j.size==0) return true;
		else return false;
	}

	// ----------------------------------------DESTORYING MAP/BODIES-----------------------------------------------------------------
	public void entferneBloecke() { // Map destruction + advances the gamestate
		
		bodies = collisionHandler.getRemoveMap();

		for (int i = 0; i < bodies.size; i++) {
			Body b = bodies.get(i);
			map.deleteTile(b.getPosition().x , b.getPosition().y);
			world.destroyBody(b);				
			b.setUserData(null);
			b = null;

			if (i == bodies.size-1) {
				//Timer for delay camera focus changing by Siyu
				Timer.schedule(new Task(){
					@Override
					public void run() {
						advanceGameState();
					}
				}, 2);
			}
		}

		bodies.clear();
	}
	
	public void entferneBloeckeDurchSentry() { 
			
			bodies2 = collisionHandler.getRemoveMap2();
			
	
			for (int i = 0; i < bodies2.size; i++) {
				Body b = bodies2.get(i);
	
					map.deleteTile(b.getPosition().x , b.getPosition().y);
					world.destroyBody(b);
			}
			bodies2.clear();
			deleteBool = false;
		}
		
	public void deleteMissile() {
		deleteMe.add(missile.getBody());
	}

	public void entferneBloecke2() { // Map destruction

		bodies2 = collisionHandler.getRemoveMap2();
		for (int i = 0; i < bodies2.size; i++) {
			Body b2 = bodies2.get(i);
			map.deleteTile(b2.getPosition().x , b2.getPosition().y);
			world.destroyBody(b2);				
			b2.setUserData(null);
			b2 = null;
		}

		bodies2.clear();
		
	}
	
	/* In GameLoop, checks deleteMe array if bodies like projectiles should be destroyed */
	public void deleteBodies() {
		if (deleteMe.size > 0 && !world.isLocked()) {
			for (int i = 0; i < deleteMe.size; i++) {
				if (deleteMe.get(i) != null) {
					Body b = deleteMe.get(i);
					world.destroyBody(b);
					b.setUserData(null);
					deleteMe.get(i).setUserData(null);
					b = null;
				}
			}
			deleteMe.clear(); 
		}
	}
	
	public void deleteProjectile() {
		deleteMe.add(p.getBody());
		fired = false;
	}
	
	public void deleteLuftschlag(){
		deleteMe.add(luftschlag.getBody());
		startPlane = false;
	}
	
	public void deleteAirstrikePlane(){
		System.out.println("Vor delete");
		deleteMe.add(luftschlag.getPlaneBody());
		System.out.println("nach delete");
	}
	// ----------------------------------------END GAME-----------------------------------------------------------------------
	public void checkWin(){
		playercounter = game.settings.getPlayerCounter();
		int deadp =0;
		for (int i=0; i< players.size; i++) {
			if (isEmpty(players.get(i).getWorm())) {
				deadp++;
			}else {
				winnernum = players.get(i).playerNumber;
			}

		}
		if (playercounter-deadp == 1) {
			gamenotover =false;
			setGameState(GameState.GAMEOVERPLAYERWON);
			System.out.println("Sieger ist Nummer: "+ winnernum);
			game.setScreen(new GameOverScreen(game, players.get(winnernum-1).playerName, players.get(winnernum-1).worm.first().getColor()));
		}
	}
}