package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.paluno.game.gameobjects.Player;
import de.paluno.game.gameobjects.Renderable;

public class WeaponHUD implements Renderable {
	public SpriteBatch batch;
	public Viewport viewport;
	private PlayScreen playscreen;
	private Player player;
	private Stage stage;
	private Window windowPanel;
	private final Label upperLabel, lowerLabel, amountLabel;
	private TextureRegionDrawable airstrike, bazooka, bullet, fireWeapon, jetpack, mine, minigun, teleport, 
	airstrikeFocused, bazookaFocused, bulletFocused, fireWeaponFocused, jetpackFocused, mineFocused, 
	minigunFocused, teleportFocused, sentrygun, sentrygunFocused, secret, secretFocused;
	private final TextButton buttonClose; 
	private final Button airstrikeButton, bazookaButton, bulletButton, fireWeaponButton, jetpackButton, mineButton, minigunButton, teleportButton, sentrygunButton, secretButton;
	private ButtonGroup<Button> buttonGroup;
	private int crow = 2, ccol = 0;
	private Array<Button> firstRow = new Array<Button>();
	private Array<Button> secondRow = new Array<Button>();
	private Array<Button> thirdRow = new Array<Button>();
	
	public WeaponHUD(final PlayScreen playscreen, Player player) {
		this.playscreen = playscreen;
		this.player = player;
		stage = new Stage();
		Gdx.input.setInputProcessor(stage); //wichtig fuer Inputs
		
		/* File Handle */
		Skin skin = new Skin(Gdx.files.internal("uiskin.json"));
		airstrike = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/airstrke.png"))));
		airstrikeFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/airstrkeFocused.png"))));
		bazooka = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/bazooka.png"))));
		bazookaFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/bazookaFocused.png"))));
		bullet = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/bullet.png"))));
		bulletFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/bulletFocused.png"))));
		fireWeapon = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/firestrk.png"))));
		fireWeaponFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/firestrkFocused.png"))));
		jetpack = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/jetpack.png"))));
		jetpackFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/jetpackFocused.png"))));
		mine = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/mine.png"))));
		mineFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/mineFocused.png"))));
		minigun = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/minigun.png"))));
		minigunFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/minigunFocused.png"))));
		teleport = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/teleport.png"))));
		teleportFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/teleportFocused.png"))));
		sentrygun = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/sentrygun.png"))));
		sentrygunFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/sentrygunFocused.png"))));
		secret = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/secret.png"))));
		secretFocused = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("weaponIcons/secretFocused.png"))));
		
		/* WINDOW PANEL + Close Button */
		windowPanel = new Window("Choose your weapon " + player.playerName, skin);
		buttonClose = new TextButton("X", skin);
		windowPanel.getTitleTable().add(buttonClose).height(windowPanel.getPadTop());
		windowPanel.setColor(0.4f, .2f, .2f, 0.9f);
		
		/* Labels */
		upperLabel = new Label("Weapon 1", skin);
		lowerLabel = new Label("Damage: 80", skin);
		lowerLabel.setFontScale(0.8f);
		amountLabel = new Label("Endless", skin);
		amountLabel.setFontScale(0.8f);
		
		/* BUTTONS */
		airstrikeButton = new Button(airstrike, airstrike, airstrikeFocused);
		bazookaButton = new Button(bazooka, bazooka, bazookaFocused);
		bulletButton = new Button(bullet, bullet, bulletFocused);
		fireWeaponButton = new Button(fireWeapon, fireWeapon, fireWeaponFocused);
		jetpackButton = new Button(jetpack, jetpack, jetpackFocused);
		mineButton = new Button(mine, mine, mineFocused);
		minigunButton = new Button(minigun, minigun, minigunFocused);
		teleportButton = new Button(teleport, teleport, teleportFocused);
		sentrygunButton = new Button(sentrygun, sentrygun, sentrygunFocused);
		secretButton = new Button(secret, secret, secretFocused);
		
		buttonGroup = new ButtonGroup<Button>(bazookaButton, bulletButton, minigunButton, fireWeaponButton,
				jetpackButton, mineButton, airstrikeButton, teleportButton, sentrygunButton, secretButton);
		buttonGroup.setMaxCheckCount(1);
		buttonGroup.setMinCheckCount(1);
		firstRow.add(bazookaButton, bulletButton, minigunButton);
		secondRow.add(fireWeaponButton, jetpackButton, mineButton);
		thirdRow.add(airstrikeButton, teleportButton, sentrygunButton, secretButton);
		bazookaButton.setChecked(true);
		
		/* ADD TO WINDOW PANEL */
		windowPanel.row().padBottom(10).expandX();
		windowPanel.add(upperLabel).colspan(4).center();
		
		windowPanel.row().padBottom(10).expandX();
		windowPanel.add(bazookaButton).width(40).height(40);
		windowPanel.add(bulletButton).width(40).height(40);
		windowPanel.add(minigunButton).width(40).height(40);
		
		windowPanel.row().padBottom(10);
		windowPanel.add(fireWeaponButton).width(40).height(40);
		windowPanel.add(jetpackButton).width(40).height(40);
		windowPanel.add(mineButton).width(40).height(40);
		
		windowPanel.row().padBottom(10);
		windowPanel.add(airstrikeButton).width(40).height(40);
		windowPanel.add(teleportButton).width(40).height(40);
		windowPanel.add(sentrygunButton).width(40).height(40);
		
		windowPanel.row().height(40);
		windowPanel.add(amountLabel).colspan(2);
		windowPanel.add(lowerLabel).colspan(2);
				
		/* Pack + Add + Things */
		windowPanel.pack(); //Important! Correctly scales the window after adding new elements.
		
		float newWidth = 440, newHeight = 300;
		windowPanel.setBounds(
				(Gdx.graphics.getWidth() - newWidth ) / 2,
				(Gdx.graphics.getHeight() - newHeight ) / 2, 
				newWidth, newHeight ); //Center on screen.
//		windowPanel.debug();
		
		stage.addActor(windowPanel);
		
		createListeners();
	}
	
	private void createListeners() {

		buttonClose.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				// Hide panel from playscreen
				playscreen.showWeapon = false;
			}
		});
		
		bazookaButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				upperLabel.setText("Weapon 1");
				lowerLabel.setText("Damage: 80");
				amountLabel.setText("Endless");
			}
		});
		
		bulletButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {				
				upperLabel.setText("Weapon 2");
				lowerLabel.setText("Damage: 70");
				amountLabel.setText("Endless");
			}
		});
		
		minigunButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				upperLabel.setText("Weapon 3");
				lowerLabel.setText("Damage: 60");
				amountLabel.setText("Endless");
			}
		});
		
		fireWeaponButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {	
				upperLabel.setText("Special Weapon");
				lowerLabel.setText("Damage: 10\n(4 Rounds)");
				amountLabel.setText("Amount: " + player.getWeaponCount(0));
			}
		});
		
		jetpackButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {				
				upperLabel.setText("Jetpack");
				lowerLabel.setText("Flying with\nthe Jetpack");
				amountLabel.setText("Amount: " + player.getWeaponCount(1));
			}
		});
		
		mineButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {			
				upperLabel.setText("Mine");
				lowerLabel.setText("Damage: 50");
				amountLabel.setText("Amount: " + player.getWeaponCount(2));
			}
		});
		
		airstrikeButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {			
				upperLabel.setText("Airstrike");
				lowerLabel.setText("Damage: 40");
				amountLabel.setText("Amount: " + player.getWeaponCount(3));
			}
		});
		
		teleportButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {				
				upperLabel.setText("Teleporter");
				lowerLabel.setText("Teleport away");
				amountLabel.setText("Amount: " + player.getWeaponCount(4));
			}
		});
		
		sentrygunButton.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {			
				upperLabel.setText("Sentry Gun");
				lowerLabel.setText("Damage: 10");
				amountLabel.setText("Amount: " + player.getWeaponCount(5));
			}
		});
	}

	@Override
	public void render(SpriteBatch batch, float delta) {
		if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
			if (ccol < firstRow.size-1) ccol++;
			navigateMenu();
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
			if (ccol > 0) ccol--;
			navigateMenu();
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
			if (crow != 0) crow--;
			navigateMenu();
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
			if (crow != 2) crow++;
			navigateMenu();
		} 
		else if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
			playscreen.showWeapon = false;
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_1)) {
			firstRow.get(0).setChecked(true);
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_2)) {
			firstRow.get(1).setChecked(true);
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_3)) {
			firstRow.get(2).setChecked(true);
		}
		else if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_4)) {
			secondRow.get(0).setChecked(true);
		}
//		System.out.println("Row: " + crow + ", Col: " + ccol);
		
		stage.act(Math.min(delta, 1 / 30f));
		stage.draw();
	}

	private void navigateMenu() {
		if (crow == 2) {
			Button selectedButton = firstRow.get(ccol);
			selectedButton.setChecked(true);
		}
		
		else if (crow == 1) {
			Button selectedButton = secondRow.get(ccol);
			selectedButton.setChecked(true);
		}
		else {
			Button selectedButton = thirdRow.get(ccol);
			selectedButton.setChecked(true);
		}
	}
	
	public int getWeapon() {
		Array<Button> list = buttonGroup.getButtons();
		for (int i = 0; i < list.size; i++) {
			if (list.get(i).isChecked()) {
				return i+1;
			}
		}
		return 0;
	}
	
	public void setWeapon(int i) {
		Array<Button> list = buttonGroup.getButtons();
		Button b = list.get(i-1);
		b.setChecked(true);
	}
}
