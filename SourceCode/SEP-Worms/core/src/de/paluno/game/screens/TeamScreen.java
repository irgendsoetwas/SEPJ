package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import de.paluno.game.SEPGame;

public class TeamScreen extends com.badlogic.gdx.ScreenAdapter{


	private Stage stage;
	public BitmapFont font;
	public Table table;

	public Label.LabelStyle font2;

	private Skin skin;
	final SEPGame game;
	int player;
	int worm;
	public Array <TextField> text  = new Array <TextField>();
	private Button save;
	public String[] teamnames;
	public String[][] names;
	private boolean mitplayern;
	private boolean mitwormn;

	public TeamScreen(final SEPGame game){
		super();
		this.game = game;

		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);

		skin = new Skin(Gdx.files.internal("uiskin.json"));		
		table = new Table();
		table.setFillParent(true);

		font2 = new Label.LabelStyle(new BitmapFont(), Color.CHARTREUSE);
		player = game.settings.getPlayerCounter();
		worm = game.settings.getWormCounter()+2;
		mitplayern = game.settings.getMitPlayern();
		mitwormn = game.settings.getMitWormn();

		save = new TextButton("Weiter", skin);
		save.setDisabled(false);

		teamnames = new String[player];
		names = new String[game.settings.getPlayerCounter()][game.settings.getWormCounter()];

		stage.addActor(table);

		for (int row =0;row < worm; row++) {
			for (int col=0;col < player;col++) {
				if (row == 0) {
					Label one = new Label ("Team "+(col+1),font2);
					table.add(one);
				}
				else {

					TextField t = new TextField("",skin);
					if (mitplayern) {
						if (row == 1) {
							t.setMessageText("Teamname");
							text.add(t);
							table.add(t).padLeft(10);
						}
					}

					if (mitwormn) {
						if (row == 2) {
							t.setMessageText("Worm 1");
							text.add(t);
							table.add(t).padLeft(10);
						}
						else if (row == 3) {
							t.setMessageText("Worm 2");
							text.add(t);
							table.add(t).padLeft(10);
						}
						else if (row == 4) {
							t.setMessageText("Worm 3");
							text.add(t);
							table.add(t).padLeft(10);
						}
						else if (row == 5) {
							t.setMessageText("Worm 4");
							text.add(t);
							table.add(t).padLeft(10);
						}
						else if (row == 6) {
							t.setMessageText("Worm 5");
							text.add(t);
							table.add(t).padLeft(10);
						}
					}


				}

			}
			table.row().padBottom(10);
		}

		table.add(save).width(125).height(50).colspan(6);

		save.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				game.setScreen(new MenuScreen(game));

				if (mitplayern) {
					for (int a=0; a < player; a++) {

						teamnames[a]=text.get(a).getText();
						System.out.println(teamnames[a]);
					}

					for (int a=0; a < player; a++) {
						text.removeIndex(0);
					}

				}


				if (mitwormn) {
					int p = game.settings.getPlayerCounter();

					int tempWorm = 0;
					for (int tempPlayer = 0; tempPlayer < p; tempPlayer++) {
						for (int x = tempPlayer; x < text.size; x += p) {
							names[tempPlayer][tempWorm] = text.get(x).getText();
							tempWorm++;
						}
						tempWorm = 0;
					}
				}
			}

		});

		//		table.setDebug(true);
	}


	public void render(float delta){
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.draw();

	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	public void dispose () {
		stage.dispose();
		skin.dispose();
	}
	
	public void setNames(String[][] nameArray) {
		this.names = nameArray;
	}
	
	public void setTeamnames(String[] teamNames) {
		this.teamnames = teamNames;
	}

}