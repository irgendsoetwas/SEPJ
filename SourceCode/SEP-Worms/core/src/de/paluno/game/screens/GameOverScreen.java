package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.paluno.game.*;

public class GameOverScreen extends com.badlogic.gdx.ScreenAdapter{
	public SpriteBatch batch;
	public SEPGame game;		
//	WinningPlayer winnningPlayer;
	public Viewport  viewport;
	private Stage stage;
	private String winnername;
	
	public GameOverScreen (SEPGame game , String winnername, Color color){
		super();
		this.game = game;
//		this.winnningPlayer = winnningPlayer;
		this.winnername = winnername;
		batch = new SpriteBatch();
		viewport = new FitViewport(800, 480, new OrthographicCamera());
		stage = new Stage(viewport, batch );
		
		
		Label.LabelStyle font = new Label.LabelStyle(new BitmapFont(), color);
		Label.LabelStyle p1 = new Label.LabelStyle(new BitmapFont(), Color.RED);
		
		Table table = new Table();
		table.center();
		table.setFillParent(true);
		
		Label gameOverLabel = new Label ("Game over - Press Enter to restart", p1);			
		Label winner = new Label (winnername+" hat gewonnen!", font);		

		
		table.add(gameOverLabel).expandX();	
//		table.add(backtomenu).expandX();
		table.row();			
		table.add(winner);
		
	
		
//		table.add(backtomenu).expandX().pad(60F);
		stage.addActor(table); 													
	}

	public void show(){
		}
	
	public void render(float delta){
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		stage.draw();		
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
			game.setScreen(new MenuScreen(game));
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
	        Gdx.app.exit();
	}
	}
	
	public void hide(){
		
	}
	
}
