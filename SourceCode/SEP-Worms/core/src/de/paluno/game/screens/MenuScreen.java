package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import de.paluno.game.AnimationCreator;
import de.paluno.game.Animator;
import de.paluno.game.NameGenerator;
import de.paluno.game.SEPGame;

public class MenuScreen extends com.badlogic.gdx.ScreenAdapter{
	
	final SEPGame game;
	private SpriteBatch batch;
	public Viewport  viewport;
	private Stage stage;
	public BitmapFont font;
	public Label start;
	public Label.LabelStyle font1;
	public Table table;

	OrthographicCamera camera;
	Music music;
	
	private Animator animator;
	private Array<AnimationCreator> animations = new Array<AnimationCreator>();
	private int animCount = 0;
	
	public MenuScreen(SEPGame game){
		
		super();
		this.game = game;
		
		music = Gdx.audio.newMusic(Gdx.files.internal("music/PimPoyPocket.wav")); //cc-free
		music.play();
		music.setLooping(true);
		music.setVolume(0.5f);
		
		batch = new SpriteBatch();
		viewport = new FitViewport(800, 480, new OrthographicCamera());
		stage = new Stage(viewport, batch);
		Gdx.input.setInputProcessor(stage);
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		
		font1 = new Label.LabelStyle(new BitmapFont(), Color.CHARTREUSE);
		Label.LabelStyle fontred = new Label.LabelStyle(new BitmapFont(), Color.RED);
		
		table = new Table();
		table.center();
		table.setFillParent(true);
		
		start = new Label ("START = ENTER", fontred);			
		Label settings = new Label ("SETTINGS = SPACE", font1);		
		Label exit = new Label ("EXIT = ESCAPE", font1);		
		

		table.add(start).expandX().expand();		
		table.add(settings).expandX();		
		table.add(exit).expandX();

		stage.addActor(table); 			

		AnimationCreator animdead = new AnimationCreator(new Texture("wdie.png"), 60, 1, 1/50f);
		AnimationCreator animjump = new AnimationCreator(new Texture("jumpani.png"), 63, 1, 1/50f);
		AnimationCreator animbase = new AnimationCreator(new Texture("baseball.png"), 10, 1, 1/25f);
		AnimationCreator animshooter = new AnimationCreator(new Texture("shooter.png"), 10, 1, 1/25f);
		animations.add(animjump, animdead, animbase, animshooter);
		animator = new Animator(animations.get(MathUtils.random(animations.size-1)).getAnimation());
		animator.setTouchable(Touchable.enabled);
		stage.addActor(animator);
		
		animator.addListener(new InputListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				animator.changeAnimation(animations.get(animCount++).getAnimation());
				if (animCount > animations.size - 1) animCount = 0;
				return false;
			}
		});
	}
	
	public void render(float delta){
		update(delta);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();
		
		if (game.settings.kannStarten()) {
			start.setStyle(font1);
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && game.settings.kannStarten()) {
			game.setScreen(new PlayScreen(game));
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
			game.setScreen(game.settings);
			
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
		        Gdx.app.exit();
		}
		//debug ftw
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_0)) {
			game.teamscreen = new TeamScreen(game);
			NameGenerator ng = new NameGenerator(2, 3);
			game.teamscreen.setNames(ng.getNames());
			game.teamscreen.setTeamnames(ng.getTeams());
			game.settings.setMap(2);
			game.settings.setWormcounter(3);
			game.setScreen(new PlayScreen(game));
		}
		if (Gdx.input.isKeyJustPressed(Input.Keys.NUM_9)) {
			game.teamscreen = new TeamScreen(game);
			NameGenerator ng = new NameGenerator(2, 4);
			game.teamscreen.setNames(ng.getNames());
			game.teamscreen.setTeamnames(ng.getTeams());
			game.settings.setMap(3);
			game.settings.setWormcounter(4);
			game.settings.mitreplay = true;
			game.setScreen(new PlayScreen(game));
		}

		if (Gdx.input.isKeyJustPressed(Input.Keys.PERIOD)) {
			if (music.getVolume() != 0)
				music.setVolume(0);
			else
				music.setVolume(0.5f);
		}
	}
	
	public void update(float delta) {
		stage.act(delta);
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		if (!music.isPlaying())
			music.play();
	}
	
	@Override
	public void hide() {
		if (music.isPlaying()) {
			music.stop();
			music.dispose();
		}
	}
	
	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}
}
