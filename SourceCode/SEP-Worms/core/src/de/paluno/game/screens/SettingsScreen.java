package de.paluno.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import de.paluno.game.NameGenerator;
import de.paluno.game.SEPGame;

public class SettingsScreen extends com.badlogic.gdx.ScreenAdapter{

	final SEPGame game;
	private Stage stage;
	public BitmapFont font;
	public Table table;
	
	public Label.LabelStyle font2;
	public Label.LabelStyle fontred;
	
	private int wormcounter=1;
	private int playercounter = 2;
	private int map =4;
	public boolean mitreplay = false;
	private boolean mitplayern = true;
	private boolean mitwormn = true;
	
	private Label sliderlabel;
	private Label sliderlabelw;
	
	Button mapj;
	Button mapmaze;
	Button maprobo;
	Button mapmaya;
	Button save;
	Button map11;

	private TextureRegionDrawable map1,map1active,map2,map2active,map3,map3active,map4,map4active;
	
	Skin skin;

	public SettingsScreen(final SEPGame game){
		super();
		
		this.game = game;
		
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		
		skin = new Skin(Gdx.files.internal("uiskin.json"));		
		table = new Table();
		table.setFillParent(true);
		
		font2 = new Label.LabelStyle(new BitmapFont(), Color.CHARTREUSE);
		Label settings = new Label ("Wieviele Spieler?", font2);
		settings.setFontScale(2F);
		Label sliderw1 = new Label ("Wieviele W\u00fcrmer?", font2);
		sliderw1.setFontScale(2F);
		
		final Slider slider = new Slider(2, 5, 1, false, skin);
		
		final CheckBox checkBox = new CheckBox(" Replay anzeigen ? ", skin);
		checkBox.setChecked(false);
		final CheckBox checkBox2 = new CheckBox(" Teamnamen eingeben ? ", skin);
		checkBox2.setChecked(true);
		final CheckBox checkBox3 = new CheckBox(" Wurmnamen eingeben ? ", skin);
		checkBox3.setChecked(true);
		
		final Slider sliderw = new Slider(1, 5, 1, false, skin);
		
		sliderlabel = new Label (Integer.toString(playercounter), font2);
		sliderlabel.setFontScale(2F);
		
		sliderlabelw = new Label (Integer.toString(wormcounter), font2);
		sliderlabelw.setFontScale(2F);
		
		map1active = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapJ.png"))));
		map1 = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapJInactive.png"))));
		map2active = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapStone.png"))));
		map2 = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapStoneInactive.png"))));
		map3active = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapRobo.png"))));
		map3 = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapRoboInactive.png"))));
		map4active = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapMaya.png"))));
		map4 = new TextureRegionDrawable(new TextureRegion(new Texture(Gdx.files.internal("maps/mapMayaInactive.png"))));
		
		mapj = new Button(map1,map1,map1active);
		mapmaze = new Button(map2,map2,map2active);
		maprobo = new Button(map3,map3,map3active);
		mapmaya = new Button(map4,map4,map4active);
		
		ButtonGroup<Button> buttonGroup = new ButtonGroup<Button>(mapj, mapmaze, maprobo, mapmaya);
		buttonGroup.setMaxCheckCount(1);
		buttonGroup.setMinCheckCount(1);
		
		save = new TextButton("Weiter", skin);
		save.setDisabled(true);
		
		checkBox.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				mitreplay =false;
				mitreplay = checkBox.isChecked();
//				System.out.println("Replay? "+mitreplay);
			}
		});
		checkBox2.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				mitplayern = false;
				mitplayern = checkBox2.isChecked();
				System.out.println("Teamname? "+mitplayern);
			}
		});
		checkBox3.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				mitwormn = false;
				mitwormn = checkBox3.isChecked();
				System.out.println("Wormname? "+mitwormn);
			}
		});
		
		save.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				game.teamscreen = new TeamScreen(game);
				if (mitwormn == false && mitplayern == false) {
					// Hinzufuegen Namen
					NameGenerator ng = new NameGenerator(getPlayerCounter(), getWormCounter());
					game.teamscreen.setNames(ng.getNames());
					game.teamscreen.setTeamnames(ng.getTeams());
					game.setScreen(game.menu);
				}
				else if (mitwormn && mitplayern) {
					game.setScreen(game.teamscreen);
				}
				else {
					if (mitwormn == false) {
						NameGenerator ng = new NameGenerator(getPlayerCounter(), getWormCounter());
						game.teamscreen.setNames(ng.getNames());
						game.setScreen(game.teamscreen);
					}
					if (mitplayern == false) {
						NameGenerator ng = new NameGenerator(getPlayerCounter(), getWormCounter());
						game.teamscreen.setTeamnames(ng.getTeams());
						game.setScreen(game.teamscreen);
					}
				}
			}
		});
		slider.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				playercounter = (int) slider.getValue();
//				System.out.println(playercounter);
			}
		});
		sliderw.addListener(new ChangeListener() {
			public void changed (ChangeEvent event, Actor actor) {
				wormcounter = (int) sliderw.getValue();
//				System.out.println(wormcounter);	
			}
		});
		
		table.center();
		table.add(sliderw1).padRight(10F);
		table.add(sliderw).padRight(2F).width(300);
		table.add(sliderlabelw);
		table.row();
		table.add(settings).padRight(10F);
		table.add(slider).padRight(2F).width(300);
		table.add(sliderlabel);
		table.row();
		table.add(mapj).width(500).height(350);
		table.add(mapmaze).width(500).height(350);
		table.row();
		table.add(maprobo).width(500).height(350);
		table.add(mapmaya).width(500).height(350);
		table.row();
		table.add(checkBox).colspan(3);
		table.row();
		table.add(checkBox2);
		table.add(checkBox3);
		table.row();
		table.add(save).colspan(4).width(150).height(75);
		
		stage.addActor(table);

//		table.setDebug(true);
	}
	
	public void render(float delta){
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.draw();

		if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE))
			game.setScreen(new MenuScreen(game));
		
		sliderlabel.setText(Integer.toString(playercounter));
		sliderlabelw.setText(Integer.toString(wormcounter));
		
		if (mapj.isChecked()) {
			map = 0;
		}
		else if (mapmaze.isChecked()) {
			map = 1;
		}
		else if (maprobo.isChecked()) {
			map = 2;
		}
		else if (mapmaya.isChecked()) {
			map = 3;
		}
		if (kannStarten()) {
			save.setDisabled(false);
		}
	}
	
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	public void dispose () {
		stage.dispose();
		skin.dispose();
	}
	
	public boolean kannStarten() {
		if ( map < 4 ) {
			return true;
		}
		else return false;
	}
	//--------------------------Getter & Setter---------------------------------------
	public int getWormCounter() {
		return wormcounter;
	}
	
	public int getPlayerCounter() {
		return playercounter;
	}
	
	public int getMap() {
		return map;
	}
	
	public boolean getMitReplay() {
		return mitreplay;
	}
	
	public boolean getMitPlayern() {
		return mitplayern;
	}
	
	public boolean getMitWormn() {
		return mitwormn;
	}
	
	public void setWormcounter(int wcc) {
		this.wormcounter=wcc;
	}
	
	public void setMap(int m) {
		this.map=m;
	}
}
