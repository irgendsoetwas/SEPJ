package de.paluno.game.tests;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import de.paluno.game.gameobjects.Player;

public class PlayerTest {

	Player p1, p2;
	int oldValue, newValue;
	Random r;
	
	@Before
	public void setUp() throws Exception {
		p1 = new Player(1, null);
		p2 = new Player(2, null);
		oldValue = p2.getWeaponCount(1);
		newValue = 4;
		r = new Random();
	}

	@Test
	public void getWeaponsCount() {
		assertTrue(p1.getWeaponCount(0) == 3); // Sonderwaffen (weaponID: 0) sollten 3 vorhanden sein
		assertTrue(p1.getWeaponCount(2) == 4); // Mine (weaponID: 2) sollten 4 vorhanden sein
		assertTrue(p2.getWeaponCount(4) == 1); // Teleport (weaponID: 4) sollte 1 vorhanden sein
		
	}
	
	@Test
	public void setWeaponsCount() {
		p2.setWeaponCount(1, newValue);
		assertTrue(p2.getWeaponCount(1) == newValue);
		
		newValue = r.nextInt(50);
		p2.setWeaponCount(1, newValue);
		assertTrue(p2.getWeaponCount(1) == newValue);
	}
	
	@Test
	public void setWeaponsCountRandomValues() {
		newValue = r.nextInt(50);
		p2.setWeaponCount(1, newValue);
		assertTrue(p2.getWeaponCount(1) == newValue);
	}

}
