package de.paluno.game.tests;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import de.paluno.game.NameGenerator;

public class NameGeneratorTest {
	
	private NameGenerator ng, ngRand;
	private int players, worms;
	private Random r = new Random();

	@Before
	public void setUp() throws Exception {
		players = 4;
		worms = 4;
		ng = new NameGenerator(players, worms);
	}

	@Test
	public void testGetNames() {
		String[] namesArray = ng.getNames()[0];
		assertTrue(namesArray.length == worms);
	}

	@Test
	public void testGetTeams() {
		String[] teamsArray = ng.getTeams();
		assertTrue(teamsArray.length == players);
	}
	
	@Test
	public void testRandomValues() {
		int p = r.nextInt(5);
		int w = r.nextInt(5);
		
		ngRand = new NameGenerator(p,w);
		
		assertTrue(ngRand.getNames()[0].length == w);
		assertTrue(ngRand.getTeams().length == p);
	}

}
