package de.paluno.game.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ NameGeneratorTest.class, PlayerTest.class })
public class AllScreenTests {

}
