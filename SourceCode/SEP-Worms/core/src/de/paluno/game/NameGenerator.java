package de.paluno.game;

import java.util.Arrays;

import com.badlogic.gdx.utils.Array;

public class NameGenerator {

	private String[][] names = new String[][] {
		{"Neuer", "Hummels", "Boateng", "Reus", "Gomez", "Kroos", "Draxler", "Brandt"}, //0
		{"Kim-Jong", "Merkel", "Trump", "Obama", "Sarkozy", "Berlusconi", "May", "Jinping"}, //1
		{"Kafka", "Goethe", "Schiller", "Heine", "Brecht", "Fontane", "Ende", "Lessing"}, //2
		{"Beyonce", "Jay-Z", "Rihanna", "Eminem", "Avicii", "Macklemore", "Drake", "Kygo"}, //3
		{"Stallone", "Freeman", "Hanks", "Murphy", "Cruise", "Depp", "TheRock", "Pitt"}, //4
		{"Harry", "Ron", "Hermoine", "Dumbledore", "Dobby", "Ginnie", "Hagrid", "Fred"}, //5
		{"Kate", "Harry", "William", "Meghan", "Elizabeth", "Philip", "George", "Charles"}, //6
		{"Newton", "Einstein", "Curie", "Heisenberg", "Planck", "Hawking", "Darwin", "DaVinci"}, //7
		{"Picasso", "Van Gogh", "Monet", "Dali", "Renoir", "Rembrandt", "Klee", "Matisse"}, //8
		{"Mozart", "Beethoven", "Bach", "Chopin", "Wagner", "Brahns", "Haydn", "Schumann"} //9
	};
	private Array<Integer> categories = new Array<Integer>();
	private Array<Integer> nameCount = new Array<Integer>(); 
	private int playerCount, wormCount;
	private String[][] finalNameArray;
	private String[] finalTeamArray;

	public NameGenerator (int playercount, int wormcount) {
		this.playerCount = playercount;
		this.wormCount = wormcount;

		prepareArrays();

		generateArray();
	}

	private void prepareArrays() {		
		finalNameArray = new String[playerCount][wormCount];
		finalTeamArray = new String[playerCount];

		// Determining random category for each player
		for (int i = 0; i < names.length; i++) {
			categories.add(i);
		}
		categories.shuffle();

		//Shuffling the order of names
		for (int i = 0; i < names[0].length; i++) {
			nameCount.add(i);
		}
		nameCount.shuffle();
	}

	private void generateArray() {
		for (int i = 0; i < playerCount; i++) {
			finalTeamArray[i] = getCategory(categories.get(i));
			for (int j = 0; j < wormCount; j++) {
				finalNameArray[i][j] = names[categories.get(i)][nameCount.get(j)];
			}
		}
	}

	private String getCategory(int category) {
		switch (category) {
		case 0:
			return "Team Germany";
		case 1:
			return "Team Presidents";
		case 2:
			return "Team Bookworms";
		case 3:
			return "Team Musicians";
		case 4:
			return "Team Hollywood";
		case 5:
			return "Team Hogwarts";
		case 6:
			return "Team GB";
		case 7:
			return "Team Science";
		case 8:
			return "Team Art";
		case 9:
			return "Team Music";	
		default:
			return "";
		}
	}
	
	public String[][] getNames() {
		return finalNameArray;
	}
	
	public String[] getTeams() {
		return finalTeamArray;
	}

	private void debug() {
		System.out.println(Arrays.deepToString(finalNameArray));
		System.out.println(Arrays.deepToString(finalTeamArray));
	}

}
