package de.paluno.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

import de.paluno.game.screens.MenuScreen;
import de.paluno.game.screens.PlayScreen;
import de.paluno.game.screens.SettingsScreen;
import de.paluno.game.screens.TeamScreen;

public class SEPGame extends Game {
	public OrthographicCamera camera;

	public SettingsScreen settings;
	public MenuScreen menu;
	public PlayScreen playScreen;
	public TeamScreen teamscreen;
	
	public SEPGame() {
		
	}
	
	@Override
	public void create() {
		float w = Gdx.graphics.getWidth();
		float h = Gdx.graphics.getHeight();
		
		// Constructs a new OrthographicCamera, using the given window width and height
		camera = new OrthographicCamera(w, h);
		camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
		camera.update();
		
		menu = new MenuScreen(this);
		settings = new SettingsScreen(this);
		
		this.setScreen(menu);
	}

	@Override
	public void render() {
		camera.update();
		
	    super.render();
	}
	
	@Override
	public void dispose () {
		 
		 System.out.println("----- ENDE -----");

	}
}

