package de.paluno.game;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Animator extends Actor {
	Animation<TextureRegion> animation;
	TextureRegion currentRegion;
	Sprite cf;

	float time = 0f;
	
	public Animator(Animation<TextureRegion> animation) {
		this.animation = animation;
		setBounds(400, 280, animation.getKeyFrame(time, true).getRegionWidth(),animation.getKeyFrame(time, true).getRegionHeight());
	}

	@Override
	public void act(float delta){
		super.act(delta);
		time += delta;
		currentRegion = animation.getKeyFrame(time, true);
		
		cf = new Sprite(currentRegion);
		cf.setScale(1.3f);

		cf.setPosition(400, 300);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		cf.draw(batch);
	}
	
	public void changeAnimation (Animation<TextureRegion> animation) {
		this.animation = animation;
	}
}
