package de.paluno.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AnimationCreator {
	Animation<TextureRegion> animation;

	public AnimationCreator (Texture animationTexture, int FRAME_ROWS, int FRAME_COLS, float speed) {
		TextureRegion[][] tmp = TextureRegion.split(animationTexture, 
				animationTexture.getWidth() / FRAME_COLS,
				animationTexture.getHeight() / FRAME_ROWS);
		
		TextureRegion[] walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++) {
			for (int j = 0; j < FRAME_COLS; j++) {
				walkFrames[index++] = tmp[i][j];
			}
		}
		
		this.animation = new Animation<TextureRegion>(speed, walkFrames);
	}
	
	public Animation<TextureRegion> getAnimation() {
		return animation;
	}
}
