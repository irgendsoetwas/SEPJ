package de.paluno.game;

import java.util.Random;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.gameobjects.AirDrop;
import de.paluno.game.gameobjects.Boost;
import de.paluno.game.gameobjects.Luftschlag;
import de.paluno.game.gameobjects.Map;
import de.paluno.game.gameobjects.Mine;
import de.paluno.game.gameobjects.MovementBoost;
import de.paluno.game.gameobjects.Projectile;
import de.paluno.game.gameobjects.ProjectileNeu;
import de.paluno.game.gameobjects.SecretWeapon;
import de.paluno.game.gameobjects.Sonderwaffe;
import de.paluno.game.gameobjects.Water;
import de.paluno.game.gameobjects.Worm;
import de.paluno.game.screens.PlayScreen;

public class CollisionHandler implements ContactListener {
	
	PlayScreen playScreen;
	private Array<Body> removeMap = new Array<Body>(); // remove von Map-Bloecken
	private Array<Body> removeMap2 = new Array<Body>();
	Random r=new Random();
	public int x;
	Projectile projectile;
	Worm worm;
	Mine mine;
	AirDrop airdrop;
	Boost boost;
	MovementBoost movementBoost;
	boolean nichtMine = true;
	
	public CollisionHandler (PlayScreen playscreen) {
		this.playScreen = playscreen;
	}
	
	public void beginContact (Contact contact) {
		
		Body b1 = contact.getFixtureA().getBody();
		Body b2 = contact.getFixtureB().getBody();
		
		/* Worm steht auf Tiles und kann springen */
		if (b1.getUserData() instanceof Worm && b2.getUserData() instanceof Map) {
			Worm worm = (Worm) b1.getUserData();
			worm.standsOnGround = true;
		}
		if (b1.getUserData() instanceof Map && b2.getUserData() instanceof Worm) {
			Worm worm = (Worm) b2.getUserData();
			worm.standsOnGround = true;
		}
		
		/* Worm steht auf anderem Worm und kann springen */
		if (b1.getUserData() instanceof Worm && b2.getUserData() instanceof Worm) {
			Worm worm = (Worm) b1.getUserData();
			worm.standsOnGround = true;
		}
		
		 // DEBUG collsion Worm and projectile
		/* if (b1.getUserData() instanceof Worm && b2.getUserData() instanceof Projectile) {
			Worm worm = (Worm) b1.getUserData();
			System.out.println("-Last: " + playScreen.lastState);
			System.out.println("-Aktuell: " + playScreen.gamestate);
			System.out.println(worm.playerNumber);
		} 
		
		if (b1.getUserData() instanceof Projectile && b2.getUserData() instanceof Worm) {
			System.out.println("wtf");
		} */
		
		
		
		//@@@@ Collision between projectile and worm //invincible control added by Siyu
		if ((b1.getUserData() instanceof Worm && b2.getUserData() instanceof Projectile) ||
				(b1.getUserData() instanceof Projectile && b2.getUserData() instanceof Worm)) {

			if (b1.getUserData() instanceof Worm) {
				projectile=(Projectile) b2.getUserData();
				worm = (Worm) b1.getUserData();
			}
			if (b1.getUserData() instanceof Projectile) {
				projectile = (Projectile) b1.getUserData();
				worm = (Worm) b2.getUserData();
			}
			
			/* To be updated - Collision worm worm */
			if(projectile instanceof Sonderwaffe && worm.isInvincible == false) {//Judith
				worm.getroffen = true;
			}
			if(projectile instanceof SecretWeapon) {
				if(worm.isInvincible==false)
				  ((SecretWeapon) projectile).kill();
				if(worm.isInvincible==true) {
					((SecretWeapon) projectile).explode(worm);
					playScreen.deleteProjectile();
					Timer.schedule(new Task() {
						public void run() {
							playScreen.advanceGameState();
						}
					}, 1.5f);
				}
			}
			else {	
				projectile.explode();
			}
			if(worm.isInvincible == false) {
				worm.die();
			}

		}
		
		//boost collision siyu
		if (b1.getUserData() instanceof Boost && b2.getUserData() instanceof Projectile ||
				b2.getUserData() instanceof Boost && b1.getUserData() instanceof Projectile)  {
			if(b1.getUserData() instanceof Boost && b2.getUserData() instanceof Projectile)  {
				boost = (Boost)b1.getUserData();
				projectile = (Projectile)b2.getUserData();
			}
			if(b2.getUserData() instanceof Boost && b1.getUserData() instanceof Projectile)  {
				boost = (Boost)b2.getUserData();
				projectile = (Projectile)b1.getUserData();

			}	

			if(projectile.damage!=100) {
				projectile.explode();
				this.playScreen.deleteProjectile();
				boost.hp = boost.hp - projectile.damage;
				if(boost.hp <= 0) {
					this.playScreen.deleteBoost(boost);
					//	playScreen.deleteMe.add(boost.body);
					this.playScreen.currentWorm().invincibleRound = 3;
					this.playScreen.currentWorm().invincible();
					this.playScreen.currentWorm().invincibleRound--;}

				Timer.schedule(new Task(){
					@Override
					public void run() {
						playScreen.advanceGameState();
					}
				}, 2);

			}
		}
		//delete boost when sunken
		if(b1.getUserData() instanceof Water && b2.getUserData() instanceof Boost||b1.getUserData() instanceof Boost && b2.getUserData() instanceof Water) {
			if( b1.getUserData() instanceof Boost)
			    boost=(Boost)b1.getUserData();
			if( b2.getUserData() instanceof Boost)
			    boost=(Boost)b2.getUserData();
			System.out.println("boost has sunk in water" );
			Timer.schedule(new Task(){
				@Override
				public void run() {
					playScreen.deleteBoost(boost);
				}
			}, 2);
		}
		//movement boost collision siyu
		if (b1.getUserData() instanceof MovementBoost && b2.getUserData() instanceof Projectile)  {
			movementBoost = (MovementBoost)b1.getUserData();
			projectile = (Projectile)b2.getUserData();
			if(projectile.damage!=100) {
			projectile.explode();
			this.playScreen.deleteProjectile();
			movementBoost.hp = movementBoost.hp - projectile.damage;
			if(movementBoost.hp <= 0) {
				this.playScreen.deleteMovementBoost(movementBoost);
//				playScreen.deleteMe.add(movementBoost.body);
				this.playScreen.currentWorm().speedUp();
			}

			Timer.schedule(new Task(){
				@Override
				public void run() {
					playScreen.advanceGameState();
				}
			}, 2);
			}

		}
		//delete movementboost when sunken
				if(b1.getUserData() instanceof Water && b2.getUserData() instanceof MovementBoost||b1.getUserData() instanceof MovementBoost && b2.getUserData() instanceof Water) {
					if(b1.getUserData() instanceof MovementBoost)
					   movementBoost=(MovementBoost)b1.getUserData();
					if(b2.getUserData() instanceof MovementBoost)
					   movementBoost=(MovementBoost)b2.getUserData();
					System.out.println("movementboost has sunk in water" );
					Timer.schedule(new Task(){
						@Override
						public void run() {
							playScreen.deleteMovementBoost(movementBoost);
						}
					}, 2);
				}

		//Judith
		//@@@@ Collision between Mine and worm
		if ((b1.getUserData() instanceof Worm && b2.getUserData() instanceof Mine) ||
				(b1.getUserData() instanceof Mine && b2.getUserData() instanceof Worm)) {

			if (b1.getUserData() instanceof Worm) {
				mine = (Mine) b2.getUserData();
				worm = (Worm) b1.getUserData();
			}
			if (b1.getUserData() instanceof Mine) {
				mine = (Mine) b1.getUserData();
				worm = (Worm) b2.getUserData();
			}
			playScreen.mineExplodiere(mine);
			
		}	

		if (b1.getUserData() instanceof Map && b2.getUserData() instanceof Projectile)  {
			Projectile p = (Projectile) b2.getUserData();
			if(p.damage!=100) {
//				playScreen.deleteMe.add(p.getBody());
				playScreen.deleteProjectile();
				removeMap.add(b1);
				p.explode();		
			}
		}
		
		//Collision between Mine and Map:
		if (b1.getUserData() instanceof Map && b2.getUserData() instanceof Mine)  {
			Mine mine = (Mine) b2.getUserData();
			if(mine.explodiert) {
				playScreen.deleteMe.add(mine.getBody());
				removeMap2.add(b1);
			}
		}
		
		// Collision Sentry-Raketen:
				if(b2.getUserData() instanceof ProjectileNeu && b1.getUserData() instanceof Worm) {
					ProjectileNeu p = (ProjectileNeu) b2.getUserData();
					playScreen.missile = (ProjectileNeu) b2.getUserData();
					Worm w = (Worm) b1.getUserData();
					
					p.explode();
					w.dieBySentry();	
				}
				
				if (b1.getUserData() instanceof ProjectileNeu && b2.getUserData() instanceof Worm) {
					ProjectileNeu p = (ProjectileNeu) b1.getUserData();
					playScreen.missile = (ProjectileNeu) b1.getUserData();
					Worm w = (Worm) b2.getUserData();
					
					p.explode();
					w.dieBySentry();	
				}
				
				if(b2.getUserData() instanceof ProjectileNeu && b1.getUserData() instanceof Map) {
					ProjectileNeu p = (ProjectileNeu) b2.getUserData();
					playScreen.missile = (ProjectileNeu) b2.getUserData();
					
					playScreen.deleteBool = true;
					removeMap2.add(b1);
					p.explode();
				}
				
				if(b1.getUserData() instanceof ProjectileNeu && b2.getUserData() instanceof Map) {
					ProjectileNeu p = (ProjectileNeu) b1.getUserData();
					playScreen.missile = (ProjectileNeu) b1.getUserData();
					
					playScreen.deleteBool = true;
					removeMap2.add(b2);
					p.explode();
				}
			

		
		//Collision between Airstrike and Worm von Sandra
		if (b1.getUserData() instanceof Worm && b2.getUserData() instanceof Luftschlag)  {
			Luftschlag luftschlag = (Luftschlag) b2.getUserData();
			worm = (Worm) b1.getUserData();
			
			luftschlag.explode();
			worm.setHealth(worm.getHealth()-40);
			playScreen.deleteLuftschlag();
			playScreen.deleteAirstrikePlane();
			playScreen.advanceGameState();
		}

		//Collision between Airstrike and Map von Sandra
		if (b1.getUserData() instanceof Map && b2.getUserData() instanceof Luftschlag ) {
			Luftschlag luftschlag = (Luftschlag) b2.getUserData();
		
			luftschlag.explode();
			removeMap.add(b1);
			playScreen.deleteLuftschlag();
			playScreen.deleteAirstrikePlane();
		}
		
		
		//Collision between Water and Luftschlag von Sandra
		if(b1.getUserData() instanceof Water && b2.getUserData() instanceof Luftschlag){
		 	Water water = (Water) b1.getUserData();
			Luftschlag luftschlag = (Luftschlag) b2.getUserData();
			
			luftschlag.dontShow();
			playScreen.deleteLuftschlag();
			playScreen.deleteAirstrikePlane();
			playScreen.advanceGameState();
			
		}
		
		//Collision between worm and water von sandra
		if(b1.getUserData() instanceof Worm && b2.getUserData() instanceof Water){
			Water water = (Water) b2.getUserData();
			Worm worm = (Worm) b1.getUserData();
		
			worm.setHealth(0);
			worm.die();
		}
		if(b1.getUserData() instanceof Water && b2.getUserData() instanceof Worm){
		 	Water water = (Water) b1.getUserData();
			Worm worm = (Worm) b2.getUserData();

			worm.setHealth(0);
			worm.die();
		}
		
		//Collision between worm and airdrops siqi
		if(b1.getUserData() instanceof Worm&&b2.getUserData() instanceof AirDrop) {
			Worm worm=(Worm)b1.getUserData();
			AirDrop airdrop=(AirDrop)b2.getUserData();
			if(airdrop.parachuteSprite.getY()<airdrop.body.getPosition().y+10) {
				if(worm.equals(playScreen.currentWorm())) {
					if(worm.gotAirdrop==false) {
						if(airdrop.type==1) {
							worm.opened=1;
							worm.open(airdrop);
							playScreen.gotWeapon=1;
							worm.gotAirdrop=true;
							playScreen.airdropText="Found a secret weapon,press <0> in shoot mode to use";
							System.out.println("found a secret weapon");
							playScreen.deleteAirdrop(airdrop); 
//							playScreen.deleteMe.add(airdrop.body);
						}
						else if(airdrop.type==2) {
							worm.opened=1;
							worm.open(airdrop);
							worm.gotAirdrop=true;
							worm.gotMedkit();
							playScreen.airdropText="Found a med kit";
							System.out.println("found a Med Kit");
							playScreen.deleteAirdrop(airdrop);
//							playScreen.deleteMe.add(airdrop.body);
						}
						else if(airdrop.type==3) {
							worm.gotAirdrop=true;
							worm.round=3;
							worm.useBandAid();
							System.out.println("found a Band Aid");
							playScreen.airdropText="Found a Band Aid";
							playScreen.deleteAirdrop(airdrop);
//							playScreen.deleteMe.add(airdrop.body);
						}
					} 
					else {
						System.out.println("got an airdrop already" );
						playScreen.airdropText="got an airdrop already";
						playScreen.deleteAirdrop(airdrop);
//						playScreen.deleteMe.add(airdrop.body);
					}
				}
			}
	  }		
		if(b1.getUserData() instanceof Water && b2.getUserData() instanceof AirDrop||b1.getUserData() instanceof AirDrop&& b2.getUserData() instanceof Water) {
			if(b1.getUserData() instanceof AirDrop)
			   airdrop=(AirDrop)b1.getUserData();
			if(b2.getUserData() instanceof AirDrop)
			   airdrop=(AirDrop)b2.getUserData();
			airdrop.sunken=true;
			System.out.println("airdrop has sunk in water" );
		}
	}
	
	public void endContact (Contact contact) {
		//When collision ends (setting on false so one cannot jump infinitely)
		Body b1 = contact.getFixtureA().getBody();
		Body b2 = contact.getFixtureB().getBody();
		
		if (b1.getUserData() instanceof Worm && b2.getUserData() instanceof Map) {
			Worm worm = (Worm) b1.getUserData();
			if (worm.isStandsOnGround()) worm.standsOnGround = false;
		}
		
	}
	
	public void preSolve (Contact contact, Manifold mainfold) {
		
	}
	
	public void postSolve (Contact contact,ContactImpulse contactImpulse) {
		
	}

	public Array<Body> getRemoveMap() {
		return removeMap;
	}
	
	public Array<Body> getRemoveMap2() {
		return removeMap2;
	}
	
	
	public void testeArrayAufInhalt (Body b) {
		
	}
	

}

