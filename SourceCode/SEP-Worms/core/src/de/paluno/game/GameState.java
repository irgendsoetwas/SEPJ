package de.paluno.game;

public enum GameState {
    
	PLAYERTURN,
	
	SHOOTING,
	
	GAMEOVERPLAYERWON

}
