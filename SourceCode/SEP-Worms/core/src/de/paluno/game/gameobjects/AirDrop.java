package de.paluno.game.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class AirDrop implements PhysicsObject{


	public Vector2 spawn;
	private PlayScreen playscreen;
	public Body body;
	Texture airdrop=new Texture("airdrop.png");
	Sprite airdropSprite=new Sprite(airdrop);
	public int type;
	
	Texture parachute = new Texture("parachute.png");
	public Sprite parachuteSprite=new Sprite(parachute);
	
	Texture water=new Texture("water.png");
	public boolean sunken=false;
	
	
	public AirDrop(PlayScreen playscreen,int type,Vector2 spawn) {

		this.playscreen=playscreen;
		this.spawn=spawn;
		this.type=type;
		this.setupBody();
		airdropSprite.setSize(25, 25);
		airdropSprite.setOriginCenter();
		airdropSprite.setPosition(body.getPosition().x-10, body.getPosition().y-10);
		parachuteSprite.setPosition(body.getPosition().x-10,470);
	}
	
	public void setBodyToNullReference() {
		body = null;
	}
	
	public Body getBody() {
		return body;
	}
	
	public void setupBody() {

		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(spawn);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(10,10);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.friction=3000;
		
		body = playscreen.getWorld().createBody(bodyDef);
		body.createFixture(fixtureDef);
		body.setUserData(this);
		
		shape.dispose();
	}
	
	public void render(SpriteBatch batch,float delta) {
		
		if(parachuteSprite.getY()>body.getPosition().y+10)
		   parachuteSprite.draw(batch);
		if(parachuteSprite.getY()<=body.getPosition().y+10)
		    	airdropSprite.draw(batch);
		if(sunken)
			batch.draw(water, body.getPosition().x-12.5f, body.getPosition().y-12.5f, 25, 10);
	}
		
	
	public void update(float delta,GameState gamestate) {
		parachuteSprite.translate(0,-1.2f);
        airdropSprite.setPosition(body.getPosition().x-10,body.getPosition().y-10);
	}
	


}
