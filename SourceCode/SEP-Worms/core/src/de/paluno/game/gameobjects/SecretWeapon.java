package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.AnimationCreator;
import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;


public class SecretWeapon extends Projectile {
		Texture SecretWeapon = new Texture("SecretWeapon.png");
		Sprite weaponSprite = new Sprite(SecretWeapon);
		
		Texture death = new Texture("death.png");
		protected Animation<TextureRegion> killAnimation;
		TextureRegion[][] killWorm;
		private final int killRow = 11; 
		private boolean canKill=true;
		
		Texture explosion = new Texture("expow.png");
		protected Animation<TextureRegion> explodeAnimation; 
		protected AnimationCreator animation;

		public SecretWeapon(PlayScreen playScreen, Vector2 origin, Vector2 direction, int damage, int windspeed, Vector2 angle) {
			super(playScreen, origin, direction, damage, windspeed, angle);
			weaponSprite.setSize(30, 32);
			velocity.set(direction).scl(Math.min(origin.dst(direction.x, direction.y), SPEED=2));
			super.ProjectileSprite = weaponSprite;
		}
		
		public void kill() {
			killWorm = TextureRegion.split(death, 60, 60); 		

			TextureRegion[] killFrames = new TextureRegion[killRow*1];
			int index = 0;
			for (int i = 0; i < killRow; i++) { 			
				killFrames[index++] = killWorm[i][0]; 
			}
			killAnimation = new Animation<TextureRegion>(1/15f, killFrames); 
			
			visible = false;
		}
		public void explode(Worm worm) {	
			if(worm.isInvincible)
			   canKill=false;
			animation = new AnimationCreator(explosion, 12, 1, 1/15f);
			explodeAnimation = animation.getAnimation();
			visible = false;
		}
		
		public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
			
			if(visible==true) {
				ProjectileSprite.setPosition(body.getPosition().x-20, body.getPosition().y-9); 
				ProjectileSprite.draw(batch);
			}
			if(visible == false) {
				if(canKill==false) {
					stateTime += delta;
				    TextureRegion currentFrame = explodeAnimation.getKeyFrame(stateTime, false);
				    batch.draw(currentFrame, position.x, position.y);}
				if(canKill==true) {
				stateTime += delta;
				TextureRegion current = killAnimation.getKeyFrame(stateTime, false);
				batch.draw(current, position.x, position.y);}
			}
			
		}
		
		public void update(float delta, GameState gamestate) {
			if (visible) {
				position.add(velocity);		
				body.setTransform(position, 0);
			}
			
			if (out) {	
				if (position.x > 900 || position.y > 490 || position.x < 0 || position.y < 0) {

					if (gamestate == GameState.SHOOTING) {
						//Timer for delay camera focus changing by Siyu
					      playScreen.advanceGameState();

						/*Timer.schedule(new Task(){
						    @Override
						    public void run() {
							      playScreen.advanceGameState();
						    }
						}, 2);*/
						
					}
					out = false; 
				}
			}
		}
}
