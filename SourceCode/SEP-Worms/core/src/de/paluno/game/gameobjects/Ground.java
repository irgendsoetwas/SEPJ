package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.paluno.game.screens.*;

public class Ground implements Renderable, PhysicsObject {
	
	private PlayScreen playScreen;
	private Texture ground;
	private TextureRegion groundRegion;
	private Body body;
	
	public Ground (PlayScreen playscreen){
		this.playScreen = playscreen;
		
		ground = new Texture("erde.png");
		ground.setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
		groundRegion = new TextureRegion(ground, 0,0,Gdx.graphics.getWidth(), ground.getHeight()*4);
		
		this.setupBody();
	}
	
	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch s, float f) {
		s.draw(groundRegion, 0, 0);
	}
	
	public void setupBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0,0);
        
        float w = Gdx.graphics.getWidth();
        float h = 63f;
        
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(w, h);
        
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;

        body = playScreen.getWorld().createBody(bodyDef);
        body.createFixture(fixtureDef);
        body.setUserData(this);
        
        shape.dispose();
	}
	
	public Body getBody() {
		return body;
	}
	
	public void setBodyToNullReference() {
		body = null;
	}

}
