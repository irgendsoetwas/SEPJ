package de.paluno.game.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Boost implements PhysicsObject {

	private Array<Vector2> spawnPoints;
	public Vector2 spawn;
	protected PlayScreen playscreen;
	public Body body;
	Texture boost=new Texture("oscar.png");
	Sprite boostSprite=new Sprite(boost);
	public int hp =200;
	private BitmapFont font=new BitmapFont();
	
	
	
	public Boost(PlayScreen playscreen,Vector2 spawn) {
		this.playscreen=playscreen;
		this.spawn=spawn;
		this.setupBody();
		boostSprite.setSize(30, 30);
		//boostSprite.setOriginCenter();
		boostSprite.setPosition(body.getPosition().x-15, body.getPosition().y-15);
	}
	
	public Body getBody() {
		return body;
	}
	
	public void setupBody() {
		//spawnPoints = playscreen.map.getSpawnPoints();
		//spawn = spawnPoints.get(MathUtils.random(spawnPoints.size-1));
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(spawn);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(15,15);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.friction=3000;
		
		body = playscreen.getWorld().createBody(bodyDef);
		body.createFixture(fixtureDef);
		body.setUserData(this);
		
		shape.dispose();
	}
	
	public void setBodyToNullReference() {
		body = null;
	}
	
	public void render(SpriteBatch batch,float delta) {
		boostSprite.draw(batch);
		if(hp>80)
		   font.setColor(Color.GREEN);
		else font.setColor(Color.RED);
		font.draw(batch, Integer.toString(hp), body.getPosition().x, body.getPosition().y+30);
		
	}
	public void update(float delta,GameState gamestate) {
		boostSprite.setPosition(body.getPosition().x-15, body.getPosition().y-15);
	}


}