package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.screens.PlayScreen;

	public class Water implements Renderable, PhysicsObject {

		private PlayScreen playScreen;
		private Texture water;
		private TextureRegion waterRegion;
		private Body waterBody;
	
	
	
	public Water(PlayScreen playScreen){
		
		this.playScreen = playScreen;
	 
		
		water = new Texture("Water.png");
		 
		water.setWrap(Texture.TextureWrap.MirroredRepeat, Texture.TextureWrap.MirroredRepeat);
		waterRegion = new TextureRegion(water, 0,0,800, water.getHeight()*50);
			
		this.setupBody();
	}                                                                            
	public void wasserHoch(){

	      waterBody.setLinearVelocity(0.0f, 5.0f);

		Timer.schedule(new Task(){
		    @Override
		    public void run() {
			      waterBody.setLinearVelocity(0.0f, 0.0f);
		    }
		}, 1);
		

//      Timer.schedule(waterBody.setLinearVelocity(0.0f, 0f), 10f);

	}
	@Override
	public void setupBody() {
		 BodyDef bodyDef = new BodyDef();
	     bodyDef.type = BodyDef.BodyType.KinematicBody; // kinematic body, weil es sich bewegen soll, aber nicht auf andere reagieren
	     bodyDef.position.set(0,0);
	        
	    
	     
	     PolygonShape waterShape = new PolygonShape();
	     waterShape.setAsBox(waterRegion.getRegionWidth(), waterRegion.getRegionHeight()/50);	
	 
	       FixtureDef fixtureDef = new FixtureDef();
	       fixtureDef.shape = waterShape;
	     // fixtureDef.density= 997; // 
	      fixtureDef.isSensor = true;	// notwendig, damit das wasser die wuermer nicht hochtraegt
	        
	        waterBody = playScreen.getWorld().createBody(bodyDef);
	        waterBody.createFixture(fixtureDef);
	        waterBody.setUserData(this);
	        
	        
	      
	        waterShape.dispose();   
	      
	}

	@Override
	public Body getBody() {
		
		return waterBody;
	}

	@Override
	public void setBodyToNullReference() {
		waterBody= null;
		
	}

	@Override
	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch s, float delta) {
		s.draw(waterRegion, waterBody.getPosition().x , waterBody.getPosition().y - (waterRegion.getRegionHeight()) +  waterRegion.getRegionHeight()/50); // legt den Starpunkt fest
	}
}
