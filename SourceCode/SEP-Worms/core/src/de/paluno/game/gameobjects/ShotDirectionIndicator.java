package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class ShotDirectionIndicator implements Renderable{
	private Texture crosshair;
	private Sprite sprite;
	
	public float posx;
	public float posy;
	
	private float angle;
	private Vector2 distanceVector = new Vector2(30f, 0); // Laengenvektor
	private final Vector2 playerVector;
	private Vector2 crosshairVector;
	
	public ShotDirectionIndicator (Worm wurm) {
		posx = wurm.getBody().getPosition().x - wurm.wormWidth;
		posy = wurm.getBody().getPosition().y - wurm.wormHeight/4;
		
		if (wurm.flip == true) {
			angle = 0f;
		}
		else {
			angle = 180f; 
		}
	
		playerVector = new Vector2(posx, posy); // 0-Vektor der Wurmposition
//		System.out.println("Player: " + playerVector);
		
		/* Sprite des Crosshairs */
		crosshair = new Texture("crosshair.png");
		sprite = new Sprite(crosshair);
		sprite.setScale(0.7f);

		/* Position Crosshair Vektor*/
		distanceVector.setAngle(angle); 
		crosshairVector = playerVector.cpy().add(distanceVector); // 0-Vektor Crosshair = 0-Vektor Player + Lanegenvektor
	}
	
	public void update() {
		
		if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
			distanceVector.setAngle(++angle);
			crosshairVector = playerVector.cpy().add(distanceVector);
		}
		if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			distanceVector.setAngle(--angle);
			crosshairVector = playerVector.cpy().add(distanceVector);
		}
		if(Gdx.input.isKeyJustPressed(Input.Keys.F)) {
			distanceVector.setAngle(angle += 90);
			crosshairVector = playerVector.cpy().add(distanceVector);
		}
		
		sprite.setPosition(crosshairVector.x, crosshairVector.y);
	}
	
	public void render(SpriteBatch s, float f2){
		update();
		sprite.draw(s);
	}
	
	public Vector2 getAngle() {
		Vector2 vec = new Vector2(1,0);
		vec.rotate(angle);
		return vec.nor();
	}
	
	public Vector2 getPosition() {
		return crosshairVector;
	}

}
