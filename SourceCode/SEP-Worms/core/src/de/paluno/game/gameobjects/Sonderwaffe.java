package de.paluno.game.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.paluno.game.screens.PlayScreen;

public class Sonderwaffe extends Projectile {
	

	Texture sonderwaffe = new Texture("Fireball.png"); 
	Sprite sonderWaffeSprite = new Sprite(sonderwaffe);

	public Sonderwaffe(PlayScreen playScreen, Vector2 origin, Vector2 direction, int damage, int windspeed, Vector2 angle) {
		super(playScreen, origin, direction, damage, windspeed, angle);
		sonderWaffeSprite.setSize(40, 18);
		sonderWaffeSprite.rotate(direction.angle());
//		velocity.set(direction).scl(Math.min(origin.dst(direction.x, direction.y), SPEED=5));
		super.ProjectileSprite = sonderWaffeSprite;
		
//		System.out.println("Soderwaffe Koords: "+ this.SonderwaffeSprite.getX() + " " + this.SonderwaffeSprite.getY() + 
//				"  Body: " + this.body.getPosition().x + "   " +  this.body.getPosition().y);
	}
	@Override
	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
		if(visible == true) {
			ProjectileSprite.setPosition(body.getPosition().x, body.getPosition().y); 
			ProjectileSprite.draw(batch);
		}
		
	}
}
	

		

