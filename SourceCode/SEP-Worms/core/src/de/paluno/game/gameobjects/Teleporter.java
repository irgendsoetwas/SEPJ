package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.screens.PlayScreen;

public class Teleporter implements Renderable{

	private PlayScreen playScreen;
	private Texture portal;
	private TextureRegion portalRegion;

	private int posX;
	private int posY;
	public Worm worm;
		
	public Teleporter (PlayScreen playScreen, Worm worm){
		this.playScreen = playScreen;
		this.worm =worm;
		portal = new Texture("portal.png");
		portalRegion = new TextureRegion(portal, 0,0,25,35);
	}
	
	public void teleport(){
		worm.setPosition(posX,posY);
	}
	
	@Override
	public void render( SpriteBatch batch, float delta) {
		batch.draw(portalRegion, (Gdx.input.getX()*800)/Gdx.graphics.getWidth(), 480 - (Gdx.input.getY()*480)/Gdx.graphics.getHeight());
		playScreen.game.camera.position.x = playScreen.game.camera.viewportWidth/4;
		playScreen.game.camera.position.y = playScreen.game.camera.viewportHeight/4;
		playScreen.game.camera.update();
		
		if(Gdx.input.isTouched()){
			int touchX = Gdx.input.getX();
			int touchY = Gdx.input.getY();
		
			posX = touchX;
			posY = Gdx.graphics.getHeight()-touchY;
			
			posX = (Gdx.input.getX()*800)/Gdx.graphics.getWidth();
			posY = 480 - (Gdx.input.getY()*480)/Gdx.graphics.getHeight();
						
			teleport();
			playScreen.currentWorm().body.setLinearVelocity(0,-10);

			Timer.schedule(new Task(){
			    @Override
			    public void run() {
					playScreen.openPortal =false;
			    	playScreen.advanceGameState();
			    }
			}, 1
						);			
		}
	}
}
