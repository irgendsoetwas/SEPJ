package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class ProjectileNeu implements Renderable, Updatable, PhysicsObject{

	Texture projectile = new Texture("projectile.png");
	Sprite ProjectileSprite = new Sprite(projectile);
	public int x;
	public Body body;
	protected int SPEED = 10;
	protected PlayScreen playScreen;
	Texture explosion = new Texture("expow.png");

	protected Animation<TextureRegion> explodeAnimation;
	TextureRegion[][] tr;
	private final int expowRow = 12; 
	protected float stateTime;
	protected boolean out = true;

	public boolean visible = true;
	SpriteBatch batch;	

	private Vector2 origin;
	public Vector2 velocity = new Vector2();
	public Vector2 position = new Vector2();
	protected Vector2 v = new Vector2();
	public int damage;
	public int windspeed;
	public Vector2 angle;
	public Vector2 dir;
	protected boolean shoot = true;

	

	public ProjectileNeu(PlayScreen playScreen, Vector2 origin, Vector2 direct, int damage){
		this.playScreen = playScreen;
		this.origin = origin;
		this.dir = direct;
		this.damage = damage;
		this.setupBody();
		
		ProjectileSprite.rotate(direct.angle());

		velocity.set(direct).scl(Math.max(origin.dst(direct.x, direct.y), SPEED=90));
		v.set(velocity);

	}

	public void explode() {
		tr = TextureRegion.split(explosion, 60, 60); 		

		TextureRegion[] explodeFrames = new TextureRegion[expowRow*1];
		int index = 0;
		for (int i = 0; i < expowRow; i++) { 			
			explodeFrames[index++] = tr[i][0]; 
		}
		explodeAnimation = new Animation<TextureRegion>(1/15f, explodeFrames);  //1/30f -> Abspielzeit des Frames
		
		playScreen.deleteMissile();
		visible = false;
	}

	public Body getBody(){
		return body;
	}

	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
		if(visible==true) {
			ProjectileSprite.setSize(20, 5);
			ProjectileSprite.setPosition(body.getPosition().x, body.getPosition().y); 
			ProjectileSprite.draw(batch);
		}
		if(visible == false) {
			stateTime += delta;
			TextureRegion currentFrame = explodeAnimation.getKeyFrame(stateTime, false);
			batch.draw(currentFrame, position.x, position.y);
		}
	}



	public void setBodyToNullReference() { 
		//After a body is destroyed, the reference to that body object should never be used again. 
		//Because if it is, there is a good chance that the entire game will crash.
		body = null;
	}

	public void setupBody() {

		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyDef.BodyType.DynamicBody; 	 
		bodyDef.position.set(origin); 	
		body = playScreen.getWorld().createBody(bodyDef);
		PolygonShape huelle = new PolygonShape();
		huelle.setAsBox(10 ,5);
		FixtureDef fd = new FixtureDef();
		fd.shape = huelle;
		fd.density = 50;
		body.createFixture(fd);
		body.setUserData(this);
		body.setBullet(true);
		huelle.dispose();

		for (Fixture f : body.getFixtureList()) {
			f.setSensor(true); 						
		}

		position = origin;
	}

	public void update(float delta, GameState gamestate) {
	
			if (visible) {
				position.add(velocity);		
				this.body.setLinearVelocity(v);
				this.position = body.getPosition();
				shoot = false;
				
			}
		}
	
//	@Override
//	public void update(float delta, GameState gamestate) {
//		if (visible) {
//			if (shoot) {
//				position.add(velocity);
//				this.body.setLinearVelocity(v);
//				this.position = body.getPosition();
//				shoot = false;
//			}
//		}
//
//		if (out) {	
//			if (position.x > Gdx.graphics.getWidth() || position.y > Gdx.graphics.getHeight() || position.x < 0 || position.y < 0) {
//
//				if (gamestate == GameState.SHOOTING && playScreen.lastState == GameState.PLAYERONETURN) {
//					//Timer for delay camera focus changing by Siyu
//					Timer.schedule(new Task(){
//					    @Override
//					    public void run() {
//						      playScreen.advanceGameState();
//					    }
//					}, 2);
//					
//				}
//
//				else if(gamestate == GameState.SHOOTING && playScreen.lastState == GameState.PLAYERTWOTURN) {
//					//Timer for delay camera focus changing by Siyu
//					Timer.schedule(new Task(){
//					    @Override
//					    public void run() {
//						      playScreen.advanceGameState();;
//					    }
//					}, 2);
//					
//				}
//				out = false; 
//			}
//		}
//	}
}
