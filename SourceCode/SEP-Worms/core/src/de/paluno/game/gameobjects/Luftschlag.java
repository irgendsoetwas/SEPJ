package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.paluno.game.screens.PlayScreen;

public class Luftschlag implements Renderable, PhysicsObject  {
	private PlayScreen playScreen;
	private Texture plane;
	public TextureRegion planeRegion;
	private Body planeBody;
	private boolean planeEcke=false;
	private boolean pressed = false;
	private boolean firePos = false;
	private boolean exPo = false;
	
	Texture explosion = new Texture("expow.png");
	TextureRegion[][] tr;
	private final int expowRow = 12;
	protected Animation<TextureRegion> explodeAnimation;
	private boolean visible = true;
	protected float stateTime;
	public Vector2 position = new Vector2();
	
	private int posX;
	private int rockX;
	private int rockY;
	
	private Texture rocket;
	private TextureRegion rocketRegion;
	private Body rocketBody;
	
	public Luftschlag(PlayScreen playScreen){
		this.playScreen = playScreen;
		
		plane = new Texture("airplane.png");
		planeRegion = new TextureRegion(plane, 0,0,plane.getWidth(), plane.getHeight());
		
		rocket = new Texture ("projectileDown.png");
		rocketRegion = new TextureRegion (rocket,0,0,rocket.getWidth(), rocket.getHeight());

		this.setupBody();
	}

	//Flugzeug fliegt von links nach rechts wiederholend
	public void fly(){
		if (planeBody.getPosition().x< 0 || planeEcke ==false){
			if(planeBody.getPosition().x< 0){
				planeRegion.flip(true, false);
			}
			planeEcke =false;
			planeBody.setLinearVelocity(120, 0);
		}
	
		if (planeBody.getPosition().x>= 720 || planeEcke==true){
			if (planeBody.getPosition().x>= 720){
				planeRegion.flip(true, false);
			}
			planeEcke = true;
			planeBody.setLinearVelocity(-120, 0);
		}
	}
	//wird aufgerufen, wenn geschossen werden soll
	public void stopPlane(){
		planeBody.setLinearVelocity(0, 0);
		posX= (int) planeBody.getPosition().x;
		
		if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
			firePos = true;
			setupRocketBody();
		}
	}

	public void startFire (){
		rocketBody.setLinearVelocity(0, -200);
		rockX = (int) rocketBody.getPosition().x;
		rockY = (int) rocketBody.getPosition().y;
	}
	
	public void explode() {
		tr = TextureRegion.split(explosion, 60, 60); 	
		TextureRegion[] explodeFrames = new TextureRegion[expowRow*1];
		int index = 0;
		for (int i = 0; i < expowRow; i++) { 			
			explodeFrames[index++] = tr[i][0]; 
		}
		explodeAnimation = new Animation<TextureRegion>(1/15f, explodeFrames); 
		exPo= true;
		visible = false;
	}
	public void dontShow (){
		visible = false;
	}
	
	@Override
	public void setupBody() {
		 BodyDef bodyDef = new BodyDef();
	     bodyDef.type = BodyDef.BodyType.KinematicBody; // kinematic body, weil es sich bewegen soll, aber nicht auf andere reagieren

	     bodyDef.position.set(0, 500);
	     
	     PolygonShape planeShape = new PolygonShape();
	     planeShape.setAsBox(planeRegion.getRegionWidth()/2, planeRegion.getRegionHeight()/2);	
	 
	     FixtureDef fixtureDef = new FixtureDef();
	     fixtureDef.shape = planeShape;
	     fixtureDef.isSensor = true;	
	     planeBody = playScreen.getWorld().createBody(bodyDef);
	     planeBody.createFixture(fixtureDef);
	     planeBody.setUserData(this);
	     
	     planeShape.dispose(); 
	}
	
	public void setupRocketBody() {
		 BodyDef bodyDefRock = new BodyDef();
		 bodyDefRock.type = BodyDef.BodyType.DynamicBody;

		 bodyDefRock.position.set(posX,planeBody.getPosition().y);
		 
		 PolygonShape rocketShape = new PolygonShape();
	     rocketShape.setAsBox(rocketRegion.getRegionWidth()*2, rocketRegion.getRegionHeight()/2);
	     
	     FixtureDef fixtureDefRock = new FixtureDef();
	     fixtureDefRock.shape = rocketShape;
	     fixtureDefRock.isSensor = true;	
	     rocketBody = playScreen.getWorld().createBody(bodyDefRock);
	     rocketBody.createFixture(fixtureDefRock);
	     rocketBody.setUserData(this);
	     
	     rocketShape.dispose();
	}

	@Override
	public Body getBody() {
		return rocketBody;
	}

	public Body getPlaneBody(){
		return planeBody;
	}

	@Override
	public void setBodyToNullReference() {
		rocketBody=null;	
	}

	@Override
	public void render(SpriteBatch batch, float delta) {	
		playScreen.game.camera.position.x = playScreen.game.camera.viewportWidth/2/2;
		playScreen.game.camera.position.y = playScreen.game.camera.viewportHeight/1.9F/2;
		playScreen.game.camera.zoom = 0.6f;
		playScreen.game.camera.update();
		
		if (visible){
		batch.draw(planeRegion, planeBody.getPosition().x , planeBody.getPosition().y );
			if(pressed ==false){
				fly();
			}
		
			if(Gdx.input.isKeyJustPressed(Input.Keys.ENTER)){
				pressed = true;
				stopPlane();
			}
			if (firePos == true ){
				batch.draw(rocketRegion, rocketBody.getPosition().x, rocketBody.getPosition().y);
				startFire();
				}
		}
		
		if(visible == false && exPo) {
			stateTime += delta;
			TextureRegion explosion = explodeAnimation.getKeyFrame(stateTime, false);
			batch.draw(explosion, rockX	, rockY);
		}
	}
}
