package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Waffe2 extends Projectile {
	Texture waffe2 = new Texture("waffe2.png");
	Sprite waffe2Sprite = new Sprite(waffe2);
	
	public Waffe2(PlayScreen playScreen, Vector2 origin, Vector2 direction, int damage, int windspeed, Vector2 angle) {
		super(playScreen, origin, direction, damage, windspeed, angle);
		waffe2Sprite.setSize(40, 18);
		waffe2Sprite.rotate(direction.angle());
		ProjectileSprite = waffe2Sprite;
	}
	
	@Override
	public void update(float delta, GameState gamestate) {
		if (visible) {
			if (super.shoot) {
				this.body.setGravityScale(0.80f);
				v.set(velocity.add(angle.x/0.9f, angle.y/0.9f));//affected by wind and gravity in varying degrees
				this.body.setLinearVelocity(v);
				System.out.println("current velocity v = (" + v.x + "," + v.y + ")"); //check if velocity influenced by wind
				this.position = body.getPosition();
				shoot = false;
			}
		}
		if (out) {	
			if (position.x > 900 || position.y > 490 || position.x < 0 || position.y < 0) {

				if (gamestate == GameState.SHOOTING) {
					//Timer for delay camera focus changing by Siyu
					Timer.schedule(new Task(){
						@Override
						public void run() {
							playScreen.advanceGameState();
						}
					}, 2);

				}

			}
			out = false; 
		}
	}
}
