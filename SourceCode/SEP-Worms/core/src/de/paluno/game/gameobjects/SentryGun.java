package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class SentryGun implements Renderable, Updatable {

	public PlayScreen playScreen;
	
	public Texture bazookaTexture = new Texture ("Bazooka.png");
	public Sprite bazooka = new Sprite (bazookaTexture);
	
	public Animation<TextureRegion> sentryAnimationOff;
	public Animation<TextureRegion> sentryAnimationOn;
	public TextureRegion tregion;
	public Texture sentryTextureOff = new Texture("SentryOff1.png");
	public Texture sentryTextureOn = new Texture("SentryOn1.png");
	public TextureRegion[][] tr;
	public float stateTime;

	public int lifetime;
	public static int stateCounter = 0;
	public boolean triggered = false;
	public ProjectileNeu w;
	public Array<ProjectileNeu> missiles = new Array<ProjectileNeu>();
	
	public Array<ProjectileNeu> ownMissiles = new Array<ProjectileNeu>();
	
	int zufallszahl;

	public float SentryPosx;
	public float SentryPosy;
	public Worm wurm;
	public Worm target;

	public float time = 0;
	
	

	public SentryGun(PlayScreen p, Worm w1) {
		this.playScreen = p;
		this.wurm = w1;
		this.lifetime = 1;

		if (w1.flip == true) {
			SentryPosx = wurm.getBody().getPosition().x + 20;
		}
		else {
			SentryPosx = wurm.getBody().getPosition().x - 70;
		}
		
		SentryPosy = wurm.getBody().getPosition().y - 10;
		
		bazooka.setPosition(SentryPosx, SentryPosy);
		sentryAnimieren();
		
		wurm.sentryPlaced = true;
	}
	
	
	
	// Animation Verarbeitung
	public void sentryAnimieren() {
		// ANIMATION RECHTS/LINKS F?R RUHEZUSTAND
		
		if (wurm.flip == true) {
			tr = TextureRegion.split(sentryTextureOff, 50, 50);
			TextureRegion[] sentryFrames = new TextureRegion[13];

			int index = 0;
			for (int i = 0; i < 13; i++) {
				sentryFrames[index++] = tr[0][i];
			}

			sentryAnimationOff = new Animation<TextureRegion>(1 / 30f, sentryFrames);
			
		}

		else {
			tr = TextureRegion.split(sentryTextureOff, 50, 50);
			TextureRegion[] sentryFrames = new TextureRegion[13];

			int index = 0;
			for (int i = 0; i < 13; i++) {
				sentryFrames[index++] = tr[0][i];
			}

			for (int i = 0; i < sentryFrames.length; i++) {
				sentryFrames[i].flip(true, false);
			}
			
			sentryAnimationOff = new Animation<TextureRegion>(1 / 30f, sentryFrames);
			
		}

		// ANIMATION RECHTS/LINKS F?R AKTIVEN ZUSTAND
		if (wurm.flip == true) {
			tr = TextureRegion.split(sentryTextureOn, 50, 50);
			TextureRegion[] sentryFrames = new TextureRegion[13];

			int index = 0;
			for (int i = 0; i < 13; i++) {
				sentryFrames[index++] = tr[0][i];
			}

			sentryAnimationOn = new Animation<TextureRegion>(1 / 30f, sentryFrames);
			
		}

		else {
			tr = TextureRegion.split(sentryTextureOn, 50, 50);
			TextureRegion[] sentryFrames = new TextureRegion[13];

			int index = 0;
			for (int i = 0; i < 13; i++) {
				sentryFrames[index++] = tr[0][i];
			}

			for (int i = 0; i < sentryFrames.length; i++) {
				sentryFrames[i].flip(true, false);
			}
			
			sentryAnimationOn = new Animation<TextureRegion>(1 / 30f, sentryFrames);
			
		}
	}
	
	public Vector2 ermittleWurmKoords() {
		int spielerzahl = playScreen.game.settings.getPlayerCounter();
		int zufallszahl1 = (int) (Math.random() * spielerzahl);
		int zufallszahl2 = (int) (Math.random() * playScreen.players.get(zufallszahl1).worm.size);

		Worm w = playScreen.players.get(zufallszahl1).worm.get(zufallszahl2);
		
		if (playScreen.wormsp1.contains(playScreen.currentWorm(), true) && playScreen.wormsp1.contains(w, true)) {
			return ermittleWurmKoords();
		}
		else if (playScreen.wormsp2.contains(playScreen.currentWorm(), true) && playScreen.wormsp1.contains(w, true)) {
			return ermittleWurmKoords();
		}
		else if (playScreen.wormsp3.contains(playScreen.currentWorm(), true) && playScreen.wormsp1.contains(w, true)) {
			return ermittleWurmKoords();
		}
		else if (playScreen.wormsp4.contains(playScreen.currentWorm(), true) && playScreen.wormsp1.contains(w, true)) {
			return ermittleWurmKoords();
		}
		else if (playScreen.wormsp5.contains(playScreen.currentWorm(), true) && playScreen.wormsp1.contains(w, true)) {
			return ermittleWurmKoords();
		}
		else {
			
		Vector2 end = new Vector2(w.getBody().getPosition().x, w.getBody().getPosition().y);
		return end;
		}
	}
	

	public void shoot() {
		Vector2 ursprung = new Vector2(SentryPosx + 5, SentryPosy + 20);
		Vector2 ziel = ermittleWurmKoords().sub(ursprung);
		
		bazooka.setRotation(ziel.angle());
		missiles.add(new ProjectileNeu(playScreen, ursprung, ziel, 10));
	}
	

	
	
	@Override
	public void update(float delta, GameState gamestate) {
		
		if (missiles.size != 0) {
			for (ProjectileNeu m : missiles) {
				m.update(delta, gamestate);
			}
		}
	}
	
	@Override
	public void render(SpriteBatch batch, float delta) {
		renderHelpForAnimation(batch, delta);
		bazooka.draw(batch);
		

		if (missiles.size != 0) {
			for (ProjectileNeu m : missiles) {
				m.render(batch, delta);
			}
		}
	}

	public void renderHelpForAnimation(SpriteBatch batch, float delta) {
		if (playScreen.sentryHasShot == true) {
			stateTime += delta;
			TextureRegion currentFrame = sentryAnimationOn.getKeyFrame(stateTime, true);
			batch.draw(currentFrame, SentryPosx, SentryPosy);
		} else {
			stateTime += delta;
			TextureRegion currentFrame = sentryAnimationOff.getKeyFrame(stateTime, true);
			batch.draw(currentFrame, SentryPosx, SentryPosy);
		}
	}

}