package de.paluno.game.gameobjects;

public interface Renderable {
	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta);
}
