package de.paluno.game.gameobjects;

public interface PhysicsObject {
	public void setupBody();
	public com.badlogic.gdx.physics.box2d.Body getBody();
	public void setBodyToNullReference();
}
