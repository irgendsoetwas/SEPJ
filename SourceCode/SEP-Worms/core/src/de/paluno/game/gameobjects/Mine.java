package de.paluno.game.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Mine implements Renderable, Updatable, PhysicsObject{
	
	Texture mine = new Texture("mine.1.png");
	Sprite mineSprite = new Sprite(mine);
	Texture explosion = new Texture("expow.png");

	protected float stateTime;

	private Vector2 origin = new Vector2();
	public Vector2 position = new Vector2();

	public Body body;
	protected PlayScreen playScreen;
	public boolean visible = true;
	public boolean droped = false;
	public boolean explodiert = false;
	
	protected Animation<TextureRegion> explodeAnimation;
	TextureRegion[][] tr;
	private final int expowRow = 12; 

	public Mine(PlayScreen playScreen, Vector2 origin){
		this.playScreen = playScreen;
		this.origin = origin;
		mineSprite.setSize(30, 30);
		this.setupBody();
		
	}

	public void explode() {
		tr = TextureRegion.split(explosion, 60, 60); 	
		TextureRegion[] explodeFrames = new TextureRegion[expowRow*1];
		int index = 0;
		for (int i = 0; i < expowRow; i++) { 			
			explodeFrames[index++] = tr[i][0]; 
		}
		explodeAnimation = new Animation<TextureRegion>(1/15f, explodeFrames); 
		visible = false;

	}

	public Body getBody(){
		return body;
	}

	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
		if(visible==true) {
			mineSprite.setPosition(body.getPosition().x-15, body.getPosition().y-30); 
			mineSprite.draw(batch);	
		}
		if(visible == false) {
			stateTime += delta;
			TextureRegion explosion = explodeAnimation.getKeyFrame(stateTime, false);
			batch.draw(explosion, position.x, position.y);
		}
	}


	public void setBodyToNullReference() { 
		body = null;
	}

	public void setupBody() {

		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyDef.BodyType.DynamicBody; 	 
		bodyDef.position.set(origin); 	
		body = playScreen.getWorld().createBody(bodyDef);
		PolygonShape huelle = new PolygonShape();
		huelle.setAsBox(8,15);
		FixtureDef fd = new FixtureDef();
		fd.shape = huelle;
		body.createFixture(fd);
		body.setUserData(this);
		huelle.dispose();

		position = origin;
	}


//	@Override
	public void update(float delta, GameState gamestate) {

			if(droped) {
				Timer.schedule(new Task(){
				    @Override
				    public void run() {
					      playScreen.advanceGameState();;
				    }
				}, 2);

				droped = false;
			}
	}



	
}
