package de.paluno.game.gameobjects;

import com.badlogic.gdx.utils.Array;

public class Player {

	public int[] weaponsCounter = new int[6];
	public int playerNumber;
	public String playerName;
	public Array <Worm> worm;
	
	public Player (int playerNumber, Array <Worm> worms) {
		this.playerNumber = playerNumber;
		this.worm = worms;
		
		try {
			playerName = worms.get(0).getPlayername();
		}
		catch (NullPointerException e){
			playerName = "";
		}
		
		weaponsCounter[0] = 3; // Anzahl Sonderwaffe
		weaponsCounter[1] = 2; // Jetpack 
		weaponsCounter[2] = 2; // Mine
		weaponsCounter[3] = 1; // Airstrike
		weaponsCounter[4] = 1; // Teleport
		weaponsCounter[5] = 3; // Standgeschuetz
	}
	
	public int[] getWeaponCounter() {
		return weaponsCounter;
	}
	
	public int getWeaponCount(int weapon) {
		return weaponsCounter[weapon];
	}
	
	public void setWeaponCount(int weapon, int newValue) {
		weaponsCounter[weapon] = newValue;
	}
	
	public void useWeapon(int weapon) {
		weaponsCounter[weapon] = weaponsCounter[weapon] - 1;
	}
	
	public Array<Worm> getWorm() {
		return worm;
	}

	public void setWorm(Array<Worm> worm) {
		this.worm = worm;
	}

	public int getPlayerNumber() {
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) {
		this.playerNumber = playerNumber;
	}

}
