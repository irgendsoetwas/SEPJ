package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.AnimationCreator;
import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Worm extends java.lang.Object implements Renderable, Updatable, PhysicsObject {

	public boolean standsOnGround = true;

	private Array<Vector2> spawnPoints;
	private Vector2 spawn;
	public Body body;
	
	private PlayScreen playScreen;
	private Texture worm;
	private Animation<TextureRegion> walkAnimation;
	private float stateTime;
	private BitmapFont font, font2, fontFuel;
	private int numberOfWorm;
	private String wormname, playername;
	
	private int health = 100;
	public boolean flip;
	public int playerNumber;
	
	private final int FRAME_COLS = 1, FRAME_ROWS = 20;
	public float wormWidth, wormHeight;
	
	public boolean getroffen = false;  //Judith
	public int runden = 4; //Judith
	Texture fire = new Texture("firestrk.1.png"); //Judith
	
	private Texture jettex, jetpackAnimation;
	private Sprite jetSprite;
	private int initZahl;
	private Animation<TextureRegion> animationJet;
	public boolean jetpackOn = false;
	private float jetpackFuel;
	private final float fuelPrice = 0.3f;

	public boolean gotAirdrop=false;//airdrop siqi
	
	public int opened=0;
	protected Animation<TextureRegion> openAnimation;
	Texture medKit = new Texture("medKit.png");
	TextureRegion[][] foundMedKit;
	private final int medkitRow = 14; //animation for  found-med-kit siqi
	Texture weapon= new Texture("foundWeapon.png");
	TextureRegion[][] foundWeapon;
	private final int weaponRow = 7; //animation for found-secret-weapon siqi
	
	public int round=0;
	public boolean gotBandAid=true;
	Texture bandAid = new Texture("bandAid.png");
	protected Animation<TextureRegion> bandaidAnimation;
	TextureRegion[][] aid;
	private final int bandaidRow = 14; //BandAid siqi 
	
	public boolean isInvincible =false; //Siyu

	public int invincibleRound = 0; //Siyu
	public int movePoint = 500; //Siyu
	public boolean isSpeedUp = false; //Siyu
	Texture bubble = new Texture("bubble.png");
	Texture skate = new Texture("skate.png"); 
	public boolean sentryPlaced; // Gian

	
	public Worm (int playerNumber, int now, PlayScreen playScreen) {
		this.playScreen = playScreen;
		this.init(playerNumber);
		this.setupBody();
		this.numberOfWorm = now;
		this.wormname = playScreen.game.teamscreen.names[playerNumber-1][now];
		this.playername = playScreen.game.teamscreen.teamnames[playerNumber-1];
		this.sentryPlaced = false;
	}
	
	public void dieBySentry() {
		if (health <= 0) {
			playScreen.setShowReplay(true);
			playScreen.deleteWorm(this);
			playScreen.deleteMe.add(body);
		}

		else {
			health -= 10;
			if (health < 0) { 
				dieBySentry();
				}
		}
	}
	
	
	public boolean canJump() {
		if (standsOnGround) return true;
		else return false;
	}
	
	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
		checkDead();
	}

	//Boost control methods
	public void invincible() {
		isInvincible = true;
	}

	public void removeInvincible() {
		isInvincible = false;
	}

	public void speedUp() {
		this.movePoint = 1000;
		this.isSpeedUp = true;
	}

	public void checkDead() {
		if (health <=0) {
			playScreen.setShowReplay(true);
			playScreen.deleteWorm(this);
			playScreen.deleteMe.add(body);
			System.out.println("Wurmnummer:" + numberOfWorm);
			System.out.println("Wurm tot");
		}
	}
	
	public void die() {
		if (health <= 0) {
			playScreen.setShowReplay(true);
			playScreen.deleteWorm(this);
			playScreen.deleteMe.add(body);
			System.out.println("Wurmnummer:" + numberOfWorm);
			System.out.println("Wurm tot");
		}

		else {
		    health -= playScreen.p.damage;
			playScreen.deleteProjectile();
			if (health <= 0) { die(); }
			Timer.schedule(new Task() {
				public void run() {
					playScreen.advanceGameState();
				}
			}, 1.5f);
		}
	}
	
	public void open(AirDrop airdrop) {

		foundWeapon= TextureRegion.split(weapon, 25, 25); 		
		TextureRegion[] weaponFrames = new TextureRegion[weaponRow*1];
		int a = 0;
		for (int i = 0; i <weaponRow; i++) { 			
			weaponFrames[a++] = foundWeapon[i][0]; 
		}

		foundMedKit = TextureRegion.split(medKit, 25, 25); 		
		TextureRegion[] medkitFrames = new TextureRegion[medkitRow*1];
		int b = 0;
		for (int j = 0; j <medkitRow; j++) { 			
			medkitFrames[b++] = foundMedKit[j][0]; 
		}
		
		if(airdrop.type==1)
			openAnimation = new Animation<TextureRegion>(1/3f, weaponFrames); 
		if(airdrop.type==2)
	       openAnimation = new Animation<TextureRegion>(1/3f, medkitFrames); 
		
		}
	
	//got a med kit from airdrop siqi
	public void gotMedkit() {
		if(health>0&&health<=50)
			health+=50;
		else if(health>50)
			health=100;
	}
	
	//got a band-aid from airdrop siqi
	public void useBandAid() {
		if(health>0&&health<=80)
			health+=20;
		else if(health>80)
			health=100;
		this.round --;
	}
	public void bandaidAnimation() {
		aid = TextureRegion.split(bandAid, 15, 15); 		
		TextureRegion[] bandaidFrames = new TextureRegion[bandaidRow*1];
		int index = 0;
		for (int i = 0; i <bandaidRow; i++) { 			
			bandaidFrames[index++] =aid[i][0]; 
		}
		bandaidAnimation = new Animation<TextureRegion>(1/3f, bandaidFrames); 
	}
	
	public float getX() {
		return spawn.x;
	}
	public float getY() {
		return spawn.y;
	}
	
	public String getPlayername() {
		return playername;
	}
	
	public boolean isStandsOnGround() {
		return standsOnGround;
	}
	
	public int getNumberOfWorm() {
		return this.numberOfWorm;
	}
	
	private void init(int playerNumber) {
		worm = new Texture("worm1.png");
		font = new BitmapFont();
		font2 = new BitmapFont();
		this.playerNumber = playerNumber;
		
		spawnPoints = playScreen.map.getSpawnPoints();
		spawn = spawnPoints.get(MathUtils.random(spawnPoints.size-1));
		
		switch (playerNumber) {
		case 1:
			font.setColor(Color.RED);
			break;
		case 2:
			font.setColor(Color.BLUE);
			break;
		case 3:
			font.setColor(Color.GREEN);
			break;
		case 4:
			font.setColor(Color.GOLD);
			break;
		case 5:
			font.setColor(Color.PINK);
			break;
		}
		
		walkAnimation = new AnimationCreator(worm, 20, 1, 0.025f).getAnimation();
		stateTime = 0f;
		
		wormWidth = worm.getWidth()/FRAME_COLS;
		wormHeight = worm.getHeight()/FRAME_ROWS;
		
		flip = false;
	}
		
	public void setupBody() {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.position.set(spawn);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(wormWidth/2, (worm.getHeight()/FRAME_ROWS)/4);
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 0F;
		fixtureDef.restitution = 0f;
		fixtureDef.friction = 2F;
		
		body = playScreen.getWorld().createBody(bodyDef);
		body.createFixture(fixtureDef);
		body.setUserData(this);
		body.setLinearDamping(0f);
		
		shape.dispose();
	}
	
	//Teleporter
	public void setPosition(int x, int y){
		body.setTransform(x, y, 0);
	}


	@Override
	public Body getBody() {
		return body;
	}

	@Override
	public void setBodyToNullReference() {
		body = null;
	}

	@Override
	public void update(float delta, GameState gamestate) {

		if (!playScreen.showWeapon) {
			if(Gdx.input.isKeyPressed(Input.Keys.LEFT) && this.movePoint > 0) {
				body.setLinearVelocity(-70, body.getLinearVelocity().y);
//				Siyu movepoint will decrease with the movement of the worm.
				this.movePoint--;
				flip = false;
			}
			else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT) && this.movePoint > 0) {
				body.setLinearVelocity(70, body.getLinearVelocity().y);
				this.movePoint--;
				flip = true;
			}
			else {
				body.setLinearVelocity(0, body.getLinearVelocity().y);
			}

			if(Gdx.input.isKeyJustPressed(Input.Keys.UP) && canJump()) {
				standsOnGround = false;
				body.setLinearVelocity(0, 80);
			} 
		}
		
		if(!standsOnGround) {
			body.applyForceToCenter(new Vector2(0, -9.8f), true); 
		}
		
		if (jetpackOn && jetpackFuel > 0) {
			if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
				body.setLinearVelocity(-70, body.getLinearVelocity().y);
				flip = false;
				jetpackFuel -= fuelPrice;
			}
			else if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
				body.setLinearVelocity(70, body.getLinearVelocity().y);
				flip = true;
				jetpackFuel -= fuelPrice;
			}
			else if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
				body.setLinearVelocity(body.getLinearVelocity().x, 80);
				jetpackFuel -= fuelPrice;
			} 
			else {
				body.setLinearVelocity(0, -50);
			}
			playScreen.setCameraFocus(this);
		}
		
		//debug
		if (Gdx.input.isKeyJustPressed(Input.Keys.P))
			System.out.println("new Vector2(" + (int) body.getPosition().x + ", " + (int) body.getPosition().y + "), ");
	}
	
	//Judith - Sonderwaffe
	public void runden() {
		if(this.getroffen && this.runden > 0) {
		this.runden--;
		}
		else if(this.getroffen && this.runden <= 0 ) {
			this.runden = 4;
			this.getroffen = false;
		
		}
	}

	@Override
	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
		stateTime += delta;

		font.draw(batch, wormname, body.getPosition().x - wormWidth*2+wormWidth, body.getPosition().y + wormWidth*2+7);
		if (health > 25) font2.setColor(Color.GREEN);
		else font2.setColor(Color.RED);
		font2.draw(batch, Integer.toString(health), body.getPosition().x - wormWidth*2+wormWidth, body.getPosition().y + wormWidth*2-13);

		if (!jetpackOn) {
			TextureRegion currentFrame = walkAnimation.getKeyFrame(stateTime, true);
			//Direction of the Worm
			if (flip && !currentFrame.isFlipX()) currentFrame.flip(true, false);
			if (!flip && currentFrame.isFlipX()) currentFrame.flip(true, false);
			batch.draw(currentFrame, body.getPosition().x - wormWidth/2, body.getPosition().y - wormHeight/2); 
		}
		else {
			/* Start Animation of Jetpack */
			if (initZahl < 10) {
				TextureRegion currentFrame1 = animationJet.getKeyFrame(stateTime, true);
				Sprite cf1 = new Sprite(currentFrame1);
				cf1.setScale(0.8f);
				cf1.setPosition(body.getPosition().x - wormWidth/2, body.getPosition().y - wormHeight/2);
				cf1.draw(batch);
				initZahl++;
			}

			/* When Jetpack is on */
			else {
				if (flip && !jetSprite.isFlipX()) jetSprite.flip(true, false);
				if (!flip && jetSprite.isFlipX()) jetSprite.flip(true, false);
				jetSprite.setPosition(body.getPosition().x - wormWidth/2, body.getPosition().y - wormHeight/2+5);
				jetSprite.draw(batch);
				update(delta, null);
				if (jetpackFuel < 10) fontFuel.setColor(Color.RED);
				fontFuel.draw(batch, Integer.toString((int) jetpackFuel), body.getPosition().x + wormWidth, body.getPosition().y);
			}

			/* When Jetpack is empty switch to next player */
			if (jetpackFuel < 1) {
				jetpackOn = false;
				Timer.schedule(new Task(){
					@Override
					public void run() {
						playScreen.advanceGameState();;
					}
				}, 2);
			}
		}

		//Margins left and right
		if(body.getPosition().x < wormWidth*0.5) body.setTransform(wormWidth*0.5f, body.getPosition().y, 0);
		if(body.getPosition().x > 800-wormWidth*0.5) body.setTransform(800-wormWidth*0.5f, body.getPosition().y, 0);
		if (body.getPosition().y < 0) die();

		//Judith - Sonderwaffe
		if(getroffen) {
			batch.draw(fire, body.getPosition().x-12, body.getPosition().y-12, 20, 20);
		}

		//Siyu Texture
		if(isInvincible) {
			batch.draw(bubble, body.getPosition().x-22.5f, body.getPosition().y-22.5f, 45, 45);
		}
		if(isSpeedUp) {
			batch.draw(skate, body.getPosition().x-10f, body.getPosition().y-18f, 20, 10);
		}

		if(opened!=0) {//animation for found med kit or secret weapon siqi
			stateTime += delta;
			TextureRegion current= openAnimation.getKeyFrame(stateTime, true);
			batch.draw(current, body.getPosition().x+15, body.getPosition().y);
			Timer.schedule(new Task() {
				public void run() {
					opened=0;
				}
			}, 2);
		}
		if(this.round>0) {
			bandaidAnimation();
			stateTime += delta;
			TextureRegion cf= bandaidAnimation.getKeyFrame(stateTime, true);
			batch.draw(cf,body.getPosition().x+6, body.getPosition().y);
		}//animation for band aid siqi

	}

	public void startJetpack() {
		initZahl = 0;
		jetpackFuel = 100;
		jettex = new Texture("Jetpack.png");
		jetSprite = new Sprite(jettex);
		jetSprite.setScale(0.7f);
		jetpackAnimation = new Texture("JetpackAnimation.png");
		fontFuel = new BitmapFont();
		fontFuel.setColor(Color.WHITE);
		
		animationJet = new AnimationCreator(jetpackAnimation, 10, 1, 0.05f).getAnimation();
		jetpackOn = true;
	}
	
	public Color getColor() {
		return font.getColor();
	}
}

