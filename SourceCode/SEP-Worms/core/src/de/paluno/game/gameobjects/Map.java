package de.paluno.game.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;

import de.paluno.game.screens.PlayScreen;

public class Map implements Renderable, PhysicsObject {
	
	private TiledMap tileMap;
	private OrthogonalTiledMapRenderer tmr;
	private PlayScreen playScreen;
	private float tileSize;
	private Body body;
	private TiledMapTileLayer layer;
	private Array<Vector2> spawnPoints = new Array<Vector2>();
	public int mapType;
	
	public Map(PlayScreen playScreen, int map) {
		this.playScreen = playScreen;
		this.mapType = map;
		
		switch (map) {
		case 0: 
			tileMap = new TmxMapLoader().load("maps/mapJ.tmx"); 
			
			spawnPoints.addAll(
					new Vector2(62,172), new Vector2(65,107), new Vector2(152,203), new Vector2(431,139), new Vector2(101, 411), new Vector2(199, 411), new Vector2(528, 459),
					new Vector2(700, 347),new Vector2(632, 155), new Vector2(297, 395),new Vector2(497, 395),new Vector2(502, 75), new Vector2(387, 91), new Vector2(154, 410),
					new Vector2(251, 458), new Vector2(305, 458), new Vector2(385, 458), new Vector2(361, 394), new Vector2(425, 394), new Vector2(482, 394), new Vector2(535, 75), 
					new Vector2(492, 75), new Vector2(432, 90), new Vector2(621, 346), new Vector2(732, 346), new Vector2(738, 138), new Vector2(674, 154), new Vector2(782, 90), 
					new Vector2(117, 186), new Vector2(154, 202), new Vector2(206, 218), new Vector2(264, 234), new Vector2(413, 138), new Vector2(12, 106), new Vector2(265, 234), 
					new Vector2(329, 138), new Vector2(365, 138), new Vector2(398, 138), new Vector2(431, 138), new Vector2(57, 170), new Vector2(23, 106), new Vector2(62, 106), 
					new Vector2(83, 170), new Vector2(32, 170)); //44
			break;
			
		case 1:
			tileMap = new TmxMapLoader().load("maps/mapStone.tmx");
			
			spawnPoints.addAll(
					new Vector2(650,170), new Vector2(350,170), new Vector2(350,300), new Vector2(95,400), new Vector2(482,400), new Vector2(265,270),
					new Vector2(156,172), new Vector2(83,43), new Vector2(740,43), new Vector2(622,267), new Vector2(64, 394), new Vector2(115, 394), new Vector2(183, 394), 
					new Vector2(239, 394), new Vector2(292, 394), new Vector2(345, 394), new Vector2(396, 394), new Vector2(94, 42), new Vector2(174, 42), 
					new Vector2(236, 42), new Vector2(311, 42), new Vector2(383, 42), new Vector2(447, 42), new Vector2(522, 42), new Vector2(562, 42), new Vector2(130, 170), 
					new Vector2(516, 394), new Vector2(581, 394), new Vector2(649, 394), new Vector2(689, 394), new Vector2(753, 266), new Vector2(718, 266), 
					new Vector2(653, 266), new Vector2(586, 286), new Vector2(513, 266), new Vector2(708, 154), new Vector2(656, 154), new Vector2(601, 154), new Vector2(541, 154), 
					new Vector2(486, 154), new Vector2(435, 154), new Vector2(394, 154), new Vector2(487, 394), new Vector2(568, 394), new Vector2(147, 266), new Vector2(188, 266), 
					new Vector2(251, 266), new Vector2(303, 266), new Vector2(421, 266), new Vector2(464, 266), new Vector2(527, 266), new Vector2(348, 154)); //51
			break;
		
		case 2:
			tileMap = new TmxMapLoader().load("maps/mapRobo.tmx");
			
			spawnPoints.addAll(new Vector2(77, 171), new Vector2(281, 299), new Vector2(420, 331), new Vector2(343, 379), new Vector2(462, 395),
					new Vector2(610, 395), new Vector2(71, 395), new Vector2(66, 267), new Vector2(465, 171), new Vector2(306, 59), new Vector2(471, 75),
					new Vector2(740, 282), new Vector2(625, 315), new Vector2(484, 331), new Vector2(402, 58),new Vector2(374, 74),new Vector2(461, 394),
					new Vector2(531, 394),new Vector2(236, 234),new Vector2(309, 234),new Vector2(379, 234),new Vector2(441, 234),new Vector2(504, 234),
					new Vector2(577, 170), new Vector2(615, 170), new Vector2(657, 138), new Vector2(737, 282), new Vector2(689, 282), new Vector2(719, 282), 
					new Vector2(684, 234), new Vector2(515, 58), new Vector2(143, 170), new Vector2(203, 378), new Vector2(272, 378)); //34
			break;
		
		case 3: 
			tileMap = new TmxMapLoader().load("maps/map1.tmx"); 
			
			spawnPoints.addAll(new Vector2(743,267), new Vector2(666, 267), new Vector2(408, 252), new Vector2(294, 156), new Vector2(213, 76),
					new Vector2(182, 43), new Vector2(492, 171), new Vector2(561, 107), new Vector2(611, 59), new Vector2(558, 427),
					new Vector2(625, 427), new Vector2(230, 427), new Vector2(188, 427), new Vector2(536, 122), new Vector2(524, 138), new Vector2(511, 154), 
					new Vector2(461, 202), new Vector2(440, 218), new Vector2(336, 202), new Vector2(286, 154), new Vector2(272, 138), new Vector2(255, 122), 
					new Vector2(242, 106), new Vector2(227, 96), new Vector2(761, 266), new Vector2(697, 266), new Vector2(648, 266), new Vector2(630, 426), 
					new Vector2(148, 266), new Vector2(106, 266), new Vector2(73, 266), new Vector2(61, 266)); //32
			break;
		
		}
		
		tmr = new OrthogonalTiledMapRenderer(tileMap);
	
		layer = (TiledMapTileLayer) tileMap.getLayers().get("tiles");
		tileSize = layer.getTileWidth();
		
		setupBody();
	}

	@Override
	public void render(SpriteBatch batch, float delta) {

	}
	
	public TiledMapRenderer getTmr() {
		return tmr;
	}

	@Override
	public void setupBody() {
		BodyDef bdef = new BodyDef();
		FixtureDef fdef = new FixtureDef();
		
		ChainShape cs = new ChainShape();
		Vector2[] v = new Vector2[4];
		v[0] = new Vector2(-tileSize / 2, -tileSize / 2);
		v[1] = new Vector2(-tileSize / 2, tileSize / 2);
		v[2] = new Vector2(tileSize / 2, tileSize / 2);
		v[3] = new Vector2(tileSize / 2, -tileSize / 2);
		cs.createChain(v);
		
		fdef.friction = 50f;
		fdef.shape = cs;
		
		for (int row = 0; row < layer.getHeight(); row++) {
			for (int col = 0; col < layer.getWidth(); col++) {
				Cell cell = layer.getCell(col, row);
				
				if (cell == null) continue;
				if (cell.getTile() == null) continue;
				
				//create body
				bdef.type = BodyType.StaticBody;
				bdef.position.set((col+0.5f) * tileSize, (row+0.5f) * tileSize);
				
				body = playScreen.getWorld().createBody(bdef);
				body.createFixture(fdef).setUserData("block");
				body.setUserData(this);		
			}
		}
		cs.dispose();
	}

	@Override
	public Body getBody() {
		return body;
	}

	public Array<Vector2> getSpawnPoints() {
		return spawnPoints;
	}
	
	@Override
	public void setBodyToNullReference() {
		body = null;
	}
	
	public void deleteTile(float x, float y) {
		Cell cell = layer.getCell((int) x/16, (int) y/16);
		if (cell.getTile() != null) //System.out.println(cell.getTile());
		cell.setTile(null);
	}

}
