package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Waffe extends Projectile {
	Texture projectileWaffe = new Texture("projectile.png");
	Sprite waffeSprite = new Sprite(projectileWaffe);

	public Waffe(PlayScreen playScreen, Vector2 origin, Vector2 direction, int damage, int windspeed, Vector2 angle) {
		super(playScreen, origin, direction, damage, windspeed, angle);
		waffeSprite.setSize(40, 18);
		waffeSprite.rotate(direction.angle());
		velocity.set(direction).scl(Math.min(origin.dst(direction.x, direction.y), SPEED=10));
		super.ProjectileSprite = waffeSprite;
	}
	
	public void update(float delta, GameState gamestate) {
		if (visible) {
			position.add(velocity);		
			body.setTransform(position, 0);
			}
		
		if (out){	
			if (position.x > 900 || position.y > 490 || position.x < 0 || position.y < 0) {

				if (gamestate == GameState.SHOOTING) {
					playScreen.deleteProjectile();
					playScreen.advanceGameState();
				}

//				else if(gamestate == GameState.SHOOTING && playScreen.lastState == GameState.PLAYERTWOTURN) {
//					playScreen.advanceGameState();
//				}
				out = false; 
			}
		}
	}
	

	

}
