package de.paluno.game.gameobjects;

import de.paluno.game.GameState;

public interface Updatable {
	public void update(float delta, GameState gamestate);
}
