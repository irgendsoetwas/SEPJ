package de.paluno.game.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

import de.paluno.game.AnimationCreator;
import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class Projectile extends java.lang.Object implements Renderable, Updatable, PhysicsObject{

	Texture projectile = new Texture("projectile.png");
	Sprite ProjectileSprite = new Sprite(projectile);
	public int x;
	public Body body;
	protected int SPEED = 10;
	protected PlayScreen playScreen;
	Texture explosion = new Texture("expow.png");

	protected Animation<TextureRegion> explodeAnimation; 
	protected float stateTime;
	protected boolean out = true;

	public boolean visible = true;
	SpriteBatch batch;	

	private Vector2 origin;
	public Vector2 velocity = new Vector2();
	public Vector2 position = new Vector2();
	protected Vector2 v=new Vector2();
	public int damage;
	public int windspeed;
	public Vector2 angle;
	protected boolean shoot = true;
	
	protected AnimationCreator animation;

	public Projectile(PlayScreen playScreen, Vector2 origin, Vector2 direction, int damage, int windspeed, Vector2 angle){
		this.playScreen = playScreen;
		this.origin = origin.scl(1.05f);
		this.angle = angle;
		this.windspeed = windspeed;
		this.damage = damage;
		this.setupBody();
		ProjectileSprite.setSize(40, 18);
		ProjectileSprite.rotate(direction.angle());
		
//		//debug
//		windspeed = 1;
//		angle = new Vector2(0,0);

		velocity.set(direction).scl(30*Math.min(origin.dst(direction.x, direction.y), SPEED));
		angle.scl(windspeed);
		v.set(velocity.add(angle));
	}

	public void explode() {		
		animation = new AnimationCreator(explosion, 12, 1, 1/15f);
		explodeAnimation = animation.getAnimation();
		
		visible = false;
	}

	public Body getBody(){
		return body;
	}

	public void render(com.badlogic.gdx.graphics.g2d.SpriteBatch batch, float delta) {
		
		if(visible==true) {
			ProjectileSprite.setPosition(body.getPosition().x-20, body.getPosition().y-9); 
			ProjectileSprite.draw(batch);
		}
		if(visible == false) {
			stateTime += delta;
			TextureRegion currentFrame = explodeAnimation.getKeyFrame(stateTime, false);
			batch.draw(currentFrame, position.x, position.y);
		}
	}


	public void setBodyToNullReference() { 
		//After a body is destroyed, the reference to that body object should never be used again. 
		//Because if it is, there is a good chance that the entire game will crash.
		body = null;
	}

	public void setupBody() {

		BodyDef bodyDef = new BodyDef(); 
		bodyDef.type = BodyDef.BodyType.DynamicBody; 	 
		bodyDef.position.set(origin); 	
		body = playScreen.getWorld().createBody(bodyDef);
		PolygonShape huelle = new PolygonShape();
		huelle.setAsBox(10,4);
		FixtureDef fd = new FixtureDef();
		fd.shape = huelle;
		fd.density = 50;
		body.createFixture(fd);
		body.setUserData(this);
		body.setBullet(true);
		huelle.dispose();

		for (Fixture f : body.getFixtureList()) {
			f.setSensor(true); 						
		}

		position = origin;
	}

	@Override
	public void update(float delta, GameState gamestate) {
		if (visible) {
			if (shoot) {
				this.body.setLinearVelocity(v);
				System.out.println("current velocity v = (" + v.x + "," + v.y + ")"); //check if velocity influenced by wind
				this.position = body.getPosition();
				shoot = false;
			}
		}

		if (out) {	
			if (position.x > 900 || position.y > 490 || position.x < 0 || position.y < 0) {

				if (gamestate == GameState.SHOOTING) {
					//Timer for delay camera focus changing by Siyu
					Timer.schedule(new Task(){
					    @Override
					    public void run() {
					    	playScreen.deleteProjectile();
					    	playScreen.advanceGameState();
					    }
					}, 2);
					
				}
				out = false; 
			}
		}
	}
}