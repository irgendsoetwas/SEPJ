package de.paluno.game.gameobjects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import de.paluno.game.GameState;
import de.paluno.game.screens.PlayScreen;

public class WindIndicator {
	private Texture windRichtung;
	private Sprite windSprite;
	SpriteBatch batch;	
	private PlayScreen playscreen;
	Random r =new Random();
	public int speed,angel;

	public WindIndicator(PlayScreen playscreen) {
		this.playscreen=playscreen;
		windRichtung=new Texture("windRichtung.png");
		windSprite= new Sprite (windRichtung);
		windSprite.setSize(80, 20);
		windSprite.setPosition(Gdx.graphics.getWidth() - 250, Gdx.graphics.getHeight()-120);
		windSprite.setOrigin(40, 10);
		speed=r.nextInt(50)+1; //set windspeed 1-50
		angel=r.nextInt(360);
		windSprite.rotate(angel);
	}


	public void render(SpriteBatch batch,float delta) {
		windSprite.draw(batch);
		BitmapFont font = new BitmapFont();
		font.getData().setScale(1.5f);
		font.setColor(Color.YELLOW);
		font.draw(batch, "Windspeed : "+speed,  Gdx.graphics.getWidth() - 250, Gdx.graphics.getHeight()-60);
	}
  
	public void update(float delta, GameState gamestate) {
		windSprite.translate(this.getAngle().x, this.getAngle().y);
		if(Math.abs(windSprite.getX()-680)>10||Math.abs(windSprite.getY()-370)>10)
			windSprite.setPosition(Gdx.graphics.getWidth() - 250, Gdx.graphics.getHeight()-120);
	}
	
	public Vector2 getAngle() {
		Vector2 vec = new Vector2(1,0);
		vec.rotate(windSprite.getRotation());
		return vec.nor();
	}
}
