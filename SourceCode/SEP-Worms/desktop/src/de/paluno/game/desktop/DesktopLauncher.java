package de.paluno.game.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.paluno.game.SEPGame;
import de.paluno.game.UITest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "SEP Worms";
	    config.width = 800;
	    config.height = 480;
	    config.useHDPI = true;
//	    config.resizable = false;
	    config.fullscreen = true;
	    config.addIcon("icon_32.png", FileType.Internal);
//		new LwjglApplication(new UITest(), config);
	    new LwjglApplication(new SEPGame(), config);
	}
}